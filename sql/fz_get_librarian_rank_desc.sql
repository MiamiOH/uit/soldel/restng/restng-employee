/* ==========================================================================
FILE NAME:  fz_get_faculty_rank_desc.sql
Author   :  Kelly Geng
Create date:    6/21/2016
DESCRIPTION:    Get Librarian rank description         
SYSTEM:     fz_get_librarian_rank_desc.sql
 -------------------------------------------------
Copyright (c) [year(s)] Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.
     -------------------------------------------------
AUDIT TRAIL:
DATE            PRJ-TSK                         UNIQUEID        VERSION
6/21/2016    Faculty180    gengx              
Description: Original
--------------------------------------------------------------------------------- */
create or replace FUNCTION "FZ_GET_LIBRARIAN_RANK_DESC" (pidmIn number) RETURN VARCHAR2 AS
  lib_rank_desc varchar2(30);
BEGIN  
 SELECT ptvcmty_desc INTO lib_rank_desc
  FROM( SELECT * FROM ptvcmty,pprccmt
                    WHERE pprccmt_cmty_code IN ('ACL','ASL','PRL','INL')
                    AND pprccmt_pidm = pidmIn
                    AND pprccmt_cmty_code=ptvcmty_code
                    ORDER BY pprccmt_cmty_code)   
  WHERE ROWNUM = 1;
  return lib_rank_desc;
EXCEPTION
  WHEN OTHERS THEN
    return '';
END;
/
DROP PUBLIC SYNONYM FZ_GET_LIBRARIAN_RANK_DESC;
CREATE PUBLIC SYNONYM FZ_GET_LIBRARIAN_RANK_DESC FOR baninst1.FZ_GET_LIBRARIAN_RANK_DESC;