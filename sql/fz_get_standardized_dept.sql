/* ==========================================================================
FILE NAME: 	fz_get_standardized_dept.sql
Author   :  Meenakshi Kandasamy
Create date: 	5/23/2016
DESCRIPTION: 	Get standardized department code and department name 	  
SYSTEM: 	fz_get_standardized_dept.sql
 -------------------------------------------------
Copyright (c) [year(s)] Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.
     -------------------------------------------------
AUDIT TRAIL:
DATE		  UNIQUEID		
6/1/2016      kandasamy  Initial development.
10/26/2016    gengx     Added campusCode as an parameter.       			

--------------------------------------------------------------------------------- */
CREATE OR REPLACE FUNCTION "FZ_GET_STANDARDIZED_DEPT" (org_code IN varchar2,deptCodeOrName in VARCHAR2 default 'code',campusCode in VARCHAR2 default 'O') RETURN VARCHAR2 AS
  std_dept VARCHAR2(300) := null;
BEGIN  
if (deptCodeOrName = 'code') then
         select distinct(standardized_department_cd) into std_dept
         from IA_ref_organization_level_5  level5
         left outer join IA_ref_standardize_department standard_dept
         on level5.department_cd = standard_dept.department_cd
         and sysdate between standard_dept.EFFECTIVE_FROM_DT and standard_dept.EFFECTIVE_TO_DT
         where sysdate between level5.EFFECTIVE_FROM_DT and level5.EFFECTIVE_TO_DT
         and trim(organization_level_5_cd) =trim(org_code)
         and standard_dept.campus_group_cd=trim(campusCode);
elsif (deptCodeOrName ='name') then
         select distinct(standardized_department_nm) into std_dept
         from IA_ref_organization_level_5  level5
         left outer join IA_ref_standardize_department standard_dept
         on level5.department_cd = standard_dept.department_cd
         and sysdate between standard_dept.EFFECTIVE_FROM_DT and standard_dept.EFFECTIVE_TO_DT
         where sysdate between level5.EFFECTIVE_FROM_DT and level5.EFFECTIVE_TO_DT
         and trim(organization_level_5_cd) =trim(org_code)
         and standard_dept.campus_group_cd=trim(campusCode);
end if;                                                            
  return std_dept;
EXCEPTION
  WHEN OTHERS THEN
    return '';
END;
/
--REM
--REM Delete the old synonym and create the new one
--REM

DROP PUBLIC SYNONYM FZ_GET_STANDARDIZED_DEPT;
CREATE PUBLIC SYNONYM FZ_GET_STANDARDIZED_DEPT FOR baninst1.FZ_GET_STANDARDIZED_DEPT;
