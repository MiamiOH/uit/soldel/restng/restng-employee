/* ==========================================================================
FILE NAME: 	fz_get_faculty_rank_code.sql
Author   :  Meenakshi Kandasamy
Create date: 	5/23/2016
DESCRIPTION: 	Get Faculty rank code	 	  
SYSTEM: 	fz_get_faculty_rank_code.sql
 -------------------------------------------------
Copyright (c) [year(s)] Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.
     -------------------------------------------------
AUDIT TRAIL:
DATE		    PRJ-TSK                         UNIQUEID		VERSION
5/23/2016    Faculty180    kandasm				
Description: Original
--------------------------------------------------------------------------------- */
CREATE OR REPLACE FUNCTION "FZ_GET_FACULTY_RANK_CODE" (pidmIn number) RETURN VARCHAR2 AS
  fac_rank_code VARCHAR2(2) := null;
BEGIN  
  select perrank_rank_code INTO fac_rank_code
    from perrank r
    where r.perrank_pidm(+) = pidmIn
    and r.perrank_begin_date =(select max(b.perrank_begin_date) 
                               from perrank b 
                               where b.perrank_pidm = r.perrank_pidm);                                                       
  return fac_rank_code;
EXCEPTION
  WHEN OTHERS THEN
    return '';
END;
/
--REM
--REM Delete the old synonym and create the new one
--REM

DROP PUBLIC SYNONYM FZ_GET_FACULTY_RANK_CODE;
CREATE PUBLIC SYNONYM FZ_GET_FACULTY_RANK_CODE FOR baninst1.FZ_GET_FACULTY_RANK_CODE;
