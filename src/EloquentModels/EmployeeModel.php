<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/15/19
 * Time: 3:57 PM
 */

namespace MiamiOH\RestngEmployee\EloquentModels;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

class EmployeeModel extends Model
{
    /**
     * @var string $connection Connection
     */
    protected $connection = 'MUWS_GEN_PROD';
    /**
     * @var string $table Table name
     */
    public $table = 'pebempl';

    /**
     * @var bool $timestamps Do not populate auto-generated date fields
     */
    public $timestamps = false;

    /**
     * @var bool $incrementing Do not increment primary key by default
     */
    public $incrementing = false;

    /**
     * @var string $primaryKey Primary key of table
     */
    protected $primaryKey = 'pebempl_pidm';

    /**
     * @var array $guarded black list of insertable fields
     */
    protected $guarded = [];

    /**
     * Get a new query builder instance for the connection.
     * https://github.com/yajra/laravel-oci8/issues/73#issuecomment-117131744
     *
     * @return \Illuminate\Database\Query\Builder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }

}
