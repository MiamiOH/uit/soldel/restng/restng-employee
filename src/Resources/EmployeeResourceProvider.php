<?php

namespace MiamiOH\RestngEmployee\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;
use MiamiOH\RestngEmployee\Services\MassTimeEntry\Service;
use MiamiOH\RestngEmployee\Services\EmployeeReview\Service as EmployeeReviewService;

class EmployeeResourceProvider extends ResourceProvider
{

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => 'employee',
            'description' => 'Employee resources.'
        ));


        $this->addDefinition(array(
            'name' => 'Employee.Record',
            'type' => 'object',
            'properties' => array(
                'uniqueId' => array('type' => 'string', 'description' => 'Employee unique ID'),
                'pidm' => array('type' => 'string', 'description' => 'Employee PIDM'),
                'plusNumber' => array('type' => 'string', 'description' => 'Employee plus number'),
                'startDate' => array('type' => 'string', 'description' => 'Date when the employee starts working at Miami'),
                'classificationCode' => array(
                    'type' => 'string',
                    'description' => 'Employee classification code. e.g. F1|FF|AF|etc'
                ),
                'classificationDescription' => array(
                    'type' => 'string',
                    'description' => 'Description of the classification code. e.g. Faculty Full-time Temp for F1, etc.'
                ),
                'groupCode' => array(
                    'type' => 'string',
                    'description' => 'Employee group code. e.g. UP|TA|GA|etc'
                ),
                'groupDescription' => array(
                    'type' => 'string',
                    'description' => 'Description of the group code. e.g. Unclassified Police for UP etc.'
                ),
                'fullTimeCode' => array(
                    'type' => 'string',
                    'description' => 'Full-time/part-time group code. Values: FT|PT|GA|ST'
                ),
                'fullTimeDescription' => array(
                    'type' => 'string',
                    'description' => 'Full-time/part-time group description. Values: full-time, part-time, graduate assistant,student employee'
                ),
                'professionalLibrarian' => array('type' => 'boolean', 'description' => 'true|false'),
                'rankCode' => array(
                    'type' => 'string',
                    'description' => 'Rank code for professional librarians and faculty members.'
                ),
                'rankDescription' => array(
                    'type' => 'string',
                    'description' => 'Rank description for professional librarians and faculty memebers.'
                ),
                'tenureCode' => array(
                    'type' => 'string',
                    'description' => 'Tenure code for faculty memebers.Values: T|NT|TT|etc.'
                ),
                'tenureDescription' => array(
                    'type' => 'string',
                    'description' => 'Tenure description forfaculty memebers.Values: Tenured|Non-tenurable|Tenure Track|etc.'
                ),
                'CIPCode' => array(
                    'type' => 'string',
                    'description' => 'CIP code for professional librarians and faculty members.'
                ),
                'CIPDescription' => array(
                    'type' => 'string',
                    'description' => 'CIP code description for professional librarians and faculty members.'
                ),
                'gradLevelCode' => array('type' => 'string', 'description' => 'Graduate Level Standing code for faculty.'),
                'gradLevelDescription' => array(
                    'type' => 'string',
                    'description' => 'Graduate Level Standing description for faculty.'
                ),
                'gradLevelCertDate' => array(
                    'type' => 'string',
                    'description' => 'Graduate Level Standing certificate date for faculty.'
                ),
                'gradLevelExpDate' => array(
                    'type' => 'string',
                    'description' => 'Graduate Level Standing certificate expiration date for faculty.'
                ),
                'homeOrgCode' => array(
                    'type' => 'string',
                    'description' => 'Home Organization Code.'
                ),
                'homeOrgDescription' => array(
                    'type' => 'string',
                    'description' => 'Description of Home Organization Code.'
                ),
                'standardizedDivisionCode' => array(
                    'type' => 'string',
                    'description' => 'Standardized Division Code.'
                ),
                'standardizedDivisionName' => array(
                    'type' => 'string',
                    'description' => 'Standardized Division name.'
                ),
                'standardizedDepartmentCode' => array(
                    'type' => 'string',
                    'description' => 'Standardized department code.'
                ),
                'standardizedDepartmentName' => array(
                    'type' => 'string',
                    'description' => 'Standardized department name.'
                ),
                'dualAppointmentCode' => array(
                    'type' => 'string',
                    'description' => 'Dual Appointment Code for faculty appointed to two departments(e.g DENG, DBIO)'
                ),
                'person' => array(
                    'type' => 'object',
                    '$ref' => '#/definitions/Person.Record.Response',
                ),
                'jobs' => array(
                    'type' => 'collection',
                    '$ref' => '#/definitions/Job.Record.Collection',
                ),
                'includeSupplementJobs' => array(
                    'type' => 'collection',
                    '$ref' => '#/definitions/Job.Record.Collection',
                ),
            ),
        ));

        $this->addDefinition([
            'name' => 'Employee.Post',
            'type' => 'object',
            'properties' => [
                'pidm' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'employeeStatus' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'homeOrgCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'homeCoasCode' => [
                    'type' => 'string',
                ],
                'orgCodeDistribution' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'coasCodeDistribution' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'employeeClassCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'leaveCategoryCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'benefitCategoryCode' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'firstHireDate' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'currentHireDate' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'serviceDate' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'seniorityDate' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'terminatedReasonCode' => [
                    'type' => 'string',
                ],
                'workPeriodCode' => [
                    'type' => 'string',
                ],
                'flsaIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'internalFullTimePartTimeIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'districtCode' => [
                    'type' => 'string',
                ],
                'ipedsSoftMoneyIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'firstWorkDate' => [
                    'type' => 'string',
                ],
                'lastWorkDate' => [
                    'type' => 'string',
                ],
                'jobLocationCode' => [
                    'type' => 'string',
                ],
                'campusCode' => [
                    'type' => 'string',
                ],
                'collegeCode' => [
                    'type' => 'string',
                ],
                'ew2ConsentIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'ipedsPrimaryFunction' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'ipedsMedicalDentalIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'etaxConsentIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'newHireIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ],
                'tax1095ConsentIndicator' => [
                    'type' => 'string',
                    'enum'=> ['required|string'],
                ]

            ]
        ]);

        $this->addDefinition([
            'name' => 'Employee.Patch',
            'type' => 'object',
            'properties' => [
                'employeeStatus' => [
                    'type' => 'string',
                ],
                'homeOrgCode' => [
                    'type' => 'string',
                ],
                'homeCoasCode' => [
                    'type' => 'string',
                ],
                'orgCodeDistribution' => [
                    'type' => 'string',
                ],
                'coasCodeDistribution' => [
                    'type' => 'string',
                ],
                'employeeClassCode' => [
                    'type' => 'string',
                ],
                'leaveCategoryCode' => [
                    'type' => 'string',
                ],
                'benefitCategoryCode' => [
                    'type' => 'string',
                ],
                'firstHireDate' => [
                    'type' => 'string',
                ],
                'currentHireDate' => [
                    'type' => 'string',
                ],
                'serviceDate' => [
                    'type' => 'string',
                ],
                'seniorityDate' => [
                    'type' => 'string',
                ],
                'terminatedReasonCode' => [
                    'type' => 'string',
                ],
                'workPeriodCode' => [
                    'type' => 'string',
                ],
                'flsaIndicator' => [
                    'type' => 'string',
                ],
                'internalFullTimePartTimeIndicator' => [
                    'type' => 'string',
                ],
                'districtCode' => [
                    'type' => 'string',
                ],
                'ipedsSoftMoneyIndicator' => [
                    'type' => 'string',
                ],
                'firstWorkDate' => [
                    'type' => 'string',
                ],
                'lastWorkDate' => [
                    'type' => 'string',
                ],
                'jobLocationCode' => [
                    'type' => 'string',
                ],
                'campusCode' => [
                    'type' => 'string',
                ],
                'collegeCode' => [
                    'type' => 'string',
                ],
                'ew2ConsentIndicator' => [
                    'type' => 'string',
                ],
                'ipedsPrimaryFunction' => [
                    'type' => 'string',
                ],
                'ipedsMedicalDentalIndicator' => [
                    'type' => 'string',
                ],
                'etaxConsentIndicator' => [
                    'type' => 'string',
                ],
                'newHireIndicator' => [
                    'type' => 'string',
                ],
                'tax1095ConsentIndicator' => [
                    'type' => 'string',
                ]

            ]
        ]);

        $this->addDefinition([
            'name' => 'MassTimeEntry.Get',
            'type' => 'object',
            'properties' => [
                'year' => ['type' => 'string', 'enum' => ['string']],
                'payrollCode' => ['type' => 'string', 'enum' => ['string']],
                'payrollNumber' => ['type' => 'string', 'enum' => ['number']],
                'bannerId' => ['type' => 'string', 'enum' => ['string']],
                'position' => ['type' => 'string', 'enum' => ['string']],
                'suffix' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['string']],
                'earnCode' => ['type' => 'string', 'enum' => ['string']],
                'shift' => ['type' => 'string', 'enum' => ['string']],
                'beginDate' => ['type' => 'string', 'enum' => ['string']],
                'endDate' => ['type' => 'string', 'enum' => ['string']],
                'specialRate' => ['type' => 'string', 'enum' => ['number']],
                'hours' => ['type' => 'string', 'enum' => ['number']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'percent' => ['type' => 'string', 'enum' => ['number']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['string']],
                'deemedHours' => ['type' => 'string', 'enum' => ['number']],
                'activityDate' => ['type' => 'string', 'enum' => ['string']],
                'userId' => ['type' => 'string', 'enum' => ['string']],
                'dataOrigin' => ['type' => 'string', 'enum' => ['string']],
                'surrogateId' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition([
            'name' => 'MassTimeEntry.post.model',
            'type' => 'object',
            'properties' => [
                // required fields
                'year' => ['type' => 'string', 'enum' => ['required|string']],
                'payrollCode' => ['type' => 'string', 'enum' => ['required|string']],
                'payrollNumber' => ['type' => 'string', 'enum' => ['required|number']],
                'bannerId' => ['type' => 'string', 'enum' => ['required|string']],
                'beginDate' => ['type' => 'string', 'enum' => ['required|date']],
                'changeIndicator' => ['type' => 'string', 'enum' => ['required|string']],

                // non required fields
                'position' => ['type' => 'string', 'enum' => ['string']],
                'suffix' => ['type' => 'string', 'enum' => ['string']],
                'effectiveDate' => ['type' => 'string', 'enum' => ['string']],
                'earnCode' => ['type' => 'string', 'enum' => ['string']],
                'shift' => ['type' => 'string', 'enum' => ['string']],
                'endDate' => ['type' => 'string', 'enum' => ['string']],
                'specialRate' => ['type' => 'string', 'enum' => ['number']],
                'hours' => ['type' => 'string', 'enum' => ['number']],
                'accountCode' => ['type' => 'string', 'enum' => ['string']],
                'accountIndexCode' => ['type' => 'string', 'enum' => ['string']],
                'activityCode' => ['type' => 'string', 'enum' => ['string']],
                'chartOfAccountsCode' => ['type' => 'string', 'enum' => ['string']],
                'costTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'fundCode' => ['type' => 'string', 'enum' => ['string']],
                'locationCode' => ['type' => 'string', 'enum' => ['string']],
                'organizationCode' => ['type' => 'string', 'enum' => ['string']],
                'programCode' => ['type' => 'string', 'enum' => ['string']],
                'projectCode' => ['type' => 'string', 'enum' => ['string']],
                'externalAccountCode' => ['type' => 'string', 'enum' => ['string']],
                'percent' => ['type' => 'string', 'enum' => ['number']],
                'deemedHours' => ['type' => 'string', 'enum' => ['number']],

            ]
        ]);

        $this->addDefinition([
            'name' => 'EmployeeReview.Get.model',
            'type' => 'object',
            'properties' => [
                'muid' => ['type' => 'string', 'enum' => ['string']],
                'reviewTypeCode' => ['type' => 'string', 'enum' => ['string']],
                'reviewTypeDate' => ['type' => 'string', 'enum' => ['string']],
                'reviewTypeComplete' => ['type' => 'string', 'enum' => ['string']],
                'reviewTypeRating' => ['type' => 'string', 'enum' => ['string']],
                'reviewerPidm' => ['type' => 'string', 'enum' => ['string']],
                'activityDate' => ['type' => 'string', 'enum' => ['string']],
                'comment' => ['type' => 'string', 'enum' => ['string']],
                'completedDate' => ['type' => 'string', 'enum' => ['string']],
                'userId' => ['type' => 'string', 'enum' => ['string']],
                'dataOrigin' => ['type' => 'string', 'enum' => ['string']],
                'surrogateId' => ['type' => 'string', 'enum' => ['number']],
            ]
        ]);

        $this->addDefinition(array(
            'name' => 'Employee.Record.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/Employee.Record'
            )

        ));

        $this->addDefinition([
            'name' => 'Employee.Post.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Employee.Post'
            ]
        ]);


        $this->addDefinition([
            'name' => 'Employee.Patch.Collection',
            'type' => 'array',
            'items' => [
                '$ref' => '#/definitions/Employee.Put'
            ]
        ]);


    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'Employee\Employee',
            'class' => 'MiamiOH\RestngEmployee\Services\Employee',
            'description' => 'Provide API access for employees.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            ),
        ));

        $this->addService(array(
            'name' => 'Employee\EmployeeV2',
            'class' => 'MiamiOH\RestngEmployee\Services\EmployeeV2',
            'description' => 'Provide API access for employees.',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
                'bannerUtil' => array('type' => 'service', 'name' => 'MU\BannerUtil'),
            ),
        ));

        $this->addService([
            'name' => 'MassTimeEntry',
            'class' => Service::class,
            'description' => 'Provide database source',
        ]);

        $this->addService([
            'name' => 'EmployeeReview',
            'class' => EmployeeReviewService::class,
            'description' => 'Provide database source',
        ]);

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'read',
            'name' => 'employee.v1',
            'description' => 'Get collection of employee data from Banner',
            'pattern' => '/employee/v1',
            'service' => 'Employee\Employee',
            'method' => 'getEmployees',
            'returnType' => 'collection',
            'tags' => array('employee'),
            'options' => array(
                'fullTimeCode' => array(
                    'type' => 'list',
                    'description' => 'Full-time/part-time group code. e.g: FT|PT|GA|ST|IN.',
                    'enum' => ['FT', 'PT', 'GA', 'ST','IN']
                ),
                'includeOrExcludeClassCode' => array(
                    'description' => 'Include Or Exclude for classification code. The default value is include',
                    'enum' => ['include', 'exclude']),
                'classificationCode' => array(
                    'type' => 'list',
                    'description' => 'ClassificationCodes. e.g: A1,A2.'
                ),
                'gradLevel' => array(
                    'type' => 'list',
                    'description' => 'Grad Level Code. e.g: GSA|GSB.'
                ),
                'tenureCode' => array(
                    'type' => 'list',
                    'description' => 'Tenure Code. e.g: T|NT.'
                ),
                'rankCode' => array(
                    'type' => 'list',
                    'description' => 'Rank Code. e.g: ACL|PR.'
                ),
                'homeOrgCode' => array(
                    'type' => 'list',
                    'description' => 'Home Org Code. e.g: ENG99|BIO99|ART99.'
                ),
                'standardizedDivisionCode' => array(
                    'type' => 'list',
                    'description' => 'Standardized Division Code e.g: FSB|CAS|PHP.'
                ),
                'standardizedDepartmentCode' => array(
                    'type' => 'list',
                    'description' => 'Standardized Department Code e.g: CSE|ENG|EDL.'
                ),
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'Miami unique ID of the employee, or comma-separated list of unique IDs.',
                ),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'Pidm of the employee, or comma-separated list of pidms.',
                ),
                'professionalLibrarian' => array(
                    'description' => 'Filter for professional librarians.',
                    'enum' => ['true', 'false']),
                'dualAppointmentCode' => array(
                    'type' => 'list',
                    'description' => 'Dual Appointment Code e.g: DBIO|DENG|DHST.'
                ),
                'traditionalDepartment' => array(
                    'description' => 'Return traditional(Oxford) department code and names as the standardizedDepartmentCode and standardizedDepartmentName. Default value: false.',
                    'enum' => ['true', 'false']),
            ),
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,
            'isPartialable' => true,
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Read Response',
                    'returns' => array(
                        'type' => 'collection',
                        '$ref' => '#/definitions/Employee.Record.Collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    ),
                ),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Employee',
                        'key' => ['view', 'all']
                    ),

                ),

            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'employee.v1.muid',
            'description' => 'Get the employee information for a given employee.',
            'pattern' => '/employee/v1/:muid',
            'service' => 'Employee\Employee',
            'method' => 'getOneEmployee',
            'returnType' => 'model',
            'params' => array(
                'muid' => array('description' => 'ID of the employee.', 'alternateKeys' => ['uniqueId', 'pidm']),
            ),
            'options' => array(
                'traditionalDepartment' => array(
                    'description' => 'Return traditional(Oxford) department code and names as the standardizedDepartmentCode and standardizedDepartmentName. Default value: false.',
                    'enum' => ['true', 'false']),
            ),

            'isPartialable' => true,
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Read Response',
                    'returns' => array(
                        'type' => 'model',
                        '$ref' => '#/definitions/Employee.Record',
                    )
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_NOTFOUND => array(
                    'description' => 'The requested record was not found.'
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    ),
                ),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Employee',
                        'key' => ['view', 'all']
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),
        ));

        $this->addResource([
            'action' => 'create',
            'name' => 'employee.v1.create',
            'description' => 'Creates employees',
            'pattern' => '/employee/v1',
            'service' => 'Employee\Employee',
            'method' => 'createEmployee',
            'returnType' => 'model',
            'body' => [
                'description' => 'Collection of Employee Details',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Employee.Post',
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    ),
                ),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Employee',
                        'key' => ['create', 'all']
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),
        ]);


        $this->addResource([
            'action' => 'update',
            'name' => 'employee.v1.update',
            'description' => 'Update employee record',
            'pattern' => '/employee/v1/:pidm',
            'service' => 'Employee\Employee',
            'method' => 'updateEmployee',
            'returnType' => 'model',
            'body' => [
                'description' => 'Collection of Employee Details',
                'required' => true,
                'schema' => [
                    '$ref' => '#/definitions/Employee.Patch',
                ]
            ],
            'params' => [
                'pidm' => [
                    'type' => 'single',
                    'description' => 'Pidm of the employee'
                ],
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Update operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],

            ],
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    ),
                ),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Employee',
                        'key' => ['update', 'all']
                    ),
                    array(
                        'type' => 'self',
                        'param' => 'muid',
                    ),
                ),
            ),
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get mass time entry data',
            'name' => 'MassTimeEntry.read.collection',
            'service' => 'MassTimeEntry',
            'method' => 'getMassTimeEntry',
            'returnType' => 'collection',
            'tags' => ['employee'],
            'pattern' => '/employee/massTimeEntry/v1',
            'isPartiable' => true,
            'options' => [
                'year' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the mass time entry records for corresponding year'
                ],
                'payrollCode' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the mass time entry records for corresponding payroll code'
                ],
                'payrollNumber' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the mass time entry records for corresponding payroll number'
                ],
                'bannerId' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the mass time entry records for corresponding banner id'
                ],
                'beginDate' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Find records on begin date. Date Format - MM/DD/YYYY'
                ],
                'position' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records for corresponding position'
                ],
                'suffix' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records for corresponding suffix'
                ],
                'effectiveDate' => [
                    'type' => 'single',
                    'description' => 'Find records for effective date. Date Format - MM/DD/YYYY'
                ],
                'earnCode' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records for earn code'
                ],
                'shift' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records for corresponding shift'
                ],
            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of mass time entry records',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . 'MassTimeEntry.Get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Employee',
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'create',
            'description' => 'Create mass time entry',
            'name' => 'MassTimeEntry.post.single',
            'service' => 'MassTimeEntry',
            'method' => 'postSingle',
            'tags' => ['employee'],
            'pattern' => '/employee/massTimeEntry/v1',
            'body' => [
                'required' => true,
                'description' => 'mass time entry data',
                'schema' => [
                    '$ref' => '#/definitions/' . 'MassTimeEntry.post.model'
                ]
            ],
            'responses' => [
                App::API_CREATED => [
                    'description' => 'Creates or updates successfully.',
                ],
                App::API_BADREQUEST => [
                    'description' => 'Some or all data are bad.',
                ],
                App::API_FAILED => [
                    'description' => 'Insert operation failed.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Employee',
                    'key' => ['create', 'all']
                ],
            ]
        ]);

        $this->addResource([
            'action' => 'read',
            'description' => 'Get employee review data',
            'name' => 'EmployeeReview.read.collection',
            'service' => 'EmployeeReview',
            'method' => 'getEmployeeReview',
            'returnType' => 'collection',
            'tags' => ['employee'],
            'pattern' => '/employee/employeeReview/v1',
            'isPartiable' => true,
            'options' => [
                'muid' => [
                    'type' => 'single',
                    'required' => true,
                    'description' => 'Return the mass time entry records for corresponding muid. MUID can be unique id, pidm or banner id'
                ],
                'fromCompletedDate' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records from these dates review was completed. Date Format - MM/DD/YYYY'
                ],
                'toCompletedDate' => [
                    'type' => 'single',
                    'description' => 'Return the mass time entry records to these dates review was completed. Date Format - MM/DD/YYYY'
                ],

            ],
            'responses' => [
                App::API_OK => [
                    'description' => 'A collection of employee review records',
                    'returns' => [
                        'type' => 'array',
                        '$ref' => '#/definitions/' . 'EmployeeReview.Get.model',
                    ]
                ],
                App::API_NOTFOUND => [
                    'description' => 'There is no record found.',
                ],
                App::API_UNAUTHORIZED => [
                    'description' => 'Unauthorized access.',
                ],
            ],
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,
            'middleware' => [
                'authenticate' => ['type' => 'token'],
                'authorize' => [
                    'application' => 'WebServices',
                    'module' => 'Employee',
                    'key' => ['view', 'all']
                ],
            ]
        ]);

        $this->addResource(array(
            'action' => 'read',
            'name' => 'employee.v2',
            'description' => 'Get collection of employee data from Banner',
            'pattern' => '/employee/v2',
            'service' => 'Employee\EmployeeV2',
            'method' => 'getEmployees',
            'returnType' => 'collection',
            'tags' => array('employee'),
            'options' => array(
                'fullTimeCode' => array(
                    'type' => 'list',
                    'description' => 'Full-time/part-time group code. e.g: FT|PT|GA|ST|IN.',
                    'enum' => ['FT', 'PT', 'GA', 'ST','IN']
                ),
                'includeOrExcludeClassCode' => array(
                    'description' => 'Include Or Exclude for classification code. The default value is include',
                    'enum' => ['include', 'exclude']),
                'classificationCode' => array(
                    'type' => 'list',
                    'description' => 'ClassificationCodes. e.g: A1,A2.'
                ),
                'gradLevel' => array(
                    'type' => 'list',
                    'description' => 'Grad Level Code. e.g: GSA|GSB.'
                ),
                'tenureCode' => array(
                    'type' => 'list',
                    'description' => 'Tenure Code. e.g: T|NT.'
                ),
                'rankCode' => array(
                    'type' => 'list',
                    'description' => 'Rank Code. e.g: ACL|PR.'
                ),
                'homeOrgCode' => array(
                    'type' => 'list',
                    'description' => 'Home Org Code. e.g: ENG99|BIO99|ART99.'
                ),
                'standardizedDivisionCode' => array(
                    'type' => 'list',
                    'description' => 'Standardized Division Code e.g: FSB|CAS|PHP.'
                ),
                'standardizedDepartmentCode' => array(
                    'type' => 'list',
                    'description' => 'Standardized Department Code e.g: CSE|ENG|EDL.'
                ),
                'uniqueId' => array(
                    'type' => 'list',
                    'description' => 'Miami unique ID of the employee, or comma-separated list of unique IDs.',
                ),
                'pidm' => array(
                    'type' => 'list',
                    'description' => 'Pidm of the employee, or comma-separated list of pidms.',
                ),
                'professionalLibrarian' => array(
                    'description' => 'Filter for professional librarians.',
                    'enum' => ['true', 'false']),
                'dualAppointmentCode' => array(
                    'type' => 'list',
                    'description' => 'Dual Appointment Code e.g: DBIO|DENG|DHST.'
                ),
                'traditionalDepartment' => array(
                    'description' => 'Return traditional(Oxford) department code and names as the standardizedDepartmentCode and standardizedDepartmentName. Default value: false.',
                    'enum' => ['true', 'false']),
            ),
            'isPageable' => true,
            'defaultPageLimit' => 20,
            'maxPageLimit' => 500,
            'isPartialable' => true,
            'responses' => array(
                App::API_OK => array(
                    'description' => 'Read Response',
                    'returns' => array(
                        'type' => 'collection',
                        '$ref' => '#/definitions/Employee.Record.Collection',
                    )
                ),
                App::API_UNAUTHORIZED => array(
                    'description' => 'The user could not be authenticated or is not authorized to access the resource.'
                ),
                App::API_BADREQUEST => array(
                    'description' => 'The request was incorrectly formed.'
                )
            ),
            'middleware' => array(
                'authenticate' => array(
                    array(
                        'type' => 'token'
                    ),
                ),
                'authorize' => array(
                    array(
                        'type' => 'authMan',
                        'application' => 'WebServices',
                        'module' => 'Employee',
                        'key' => ['view', 'all']
                    ),

                ),

            ),
        ));
    }

    public function registerOrmConnections(): void
    {

    }
}
