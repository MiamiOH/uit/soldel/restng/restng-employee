<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-22
 * Time: 11:22
 */

namespace MiamiOH\RestngEmployee\Repositories;

class MUIDRepositoryResourceCall implements MUIDRepository
{
    /**
     * @var \Closure
     */
    private $muidCall;

    public function __construct(\Closure $muidCall)
    {
        $this->muidCall = $muidCall;
    }

    /**
     * @param array $muids
     * @return array
     * @throws \Exception
     */
    public function readMUIDs(array $muids): array
    {
        // Fetch closure (since closure is stored as property, not method)
        // or one-liner ($this->muidCall)(<params>);
        $muidCall = $this->muidCall;

        // Call closure
        $muidResponse = $muidCall(
            [
                'options' => ['muid' => $muids]
            ]
        );

        if ($muidResponse->getStatus() == '401') {
            throw new \Exception('Unauthorized to use ws-muid web service. Please request appropriate authorization and try again.');
        }

        if ($muidResponse->getStatus() == '500') {
            throw new \Exception('Sorry, ws-muid web service experienced a system error.');
        }

        return $muidResponse->getPayload();
    }

    /**
     * @param string $muid
     * @return array
     * @throws \Exception
     */
    public function readMUID(string $muid): array
    {
        $muid = strtoupper($muid);

        $payload = $this->readMUIDs([$muid]);

        if (!isset($payload[$muid])) {
            throw new \Exception("Cannot find any result for muid '$muid'.");
        }

        return $payload[$muid];
    }
}