<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-29
 * Time: 14:44
 */

namespace MiamiOH\RestngEmployee\Repositories;

use MiamiOH\RestngEmployee\EloquentModels\MassTimeEntryModel;
use MiamiOH\RestngEmployee\Objects\MassTimeEntry;

class MassTimeEntrySQL implements MassTimeEntryRepository
{
    /**
     * @param MassTimeEntry $massTimeEntry
     * @return array
     */
    public function createMassTimeEntry(MassTimeEntry $massTimeEntry): bool
    {
        MassTimeEntryModel::create([
            'phrmtim_year' => $massTimeEntry->getYear(),
            'phrmtim_pict_code' =>  $massTimeEntry->getPayrollCode(),
            'phrmtim_payno' => $massTimeEntry->getPayrollNumber(),
            'phrmtim_id' => $massTimeEntry->getBannerId(),
            'phrmtim_posn' => $massTimeEntry->getPosition(),
            'phrmtim_suff' => $massTimeEntry->getSuffix(),
            'phrmtim_effective_date' => $massTimeEntry->getEffectiveDate(),
            'phrmtim_earn_code' => $massTimeEntry->getEarnCode(),
            'phrmtim_shift' => $massTimeEntry->getShift(),
            'phrmtim_begin_date' => $massTimeEntry->getBeginDate(),
            'phrmtim_end_date' => $massTimeEntry->getEndDate(),
            'phrmtim_special_rate' => $massTimeEntry->getSpecialRate(),
            'phrmtim_hrs' => $massTimeEntry->getHours(),
            'phrmtim_activity_date' => $massTimeEntry->getActivityDate(),
            'phrmtim_coas_code' => $massTimeEntry->getChartOfAccountsCode(),
            'phrmtim_acci_code' => $massTimeEntry->getAccountIndexCode(),
            'phrmtim_fund_code' => $massTimeEntry->getFundCode(),
            'phrmtim_orgn_code' => $massTimeEntry->getOrganizationCode(),
            'phrmtim_acct_code' => $massTimeEntry->getAccountCode(),
            'phrmtim_prog_code' => $massTimeEntry->getProgramCode(),
            'phrmtim_actv_code' => $massTimeEntry->getActivityCode(),
            'phrmtim_locn_code' => $massTimeEntry->getLocationCode(),
            'phrmtim_acct_code_external' => $massTimeEntry->getExternalAccountCode(),
            'phrmtim_percent' => $massTimeEntry->getPercent(),
            'phrmtim_proj_code' => $massTimeEntry->getProjectCode(),
            'phrmtim_ctyp_code' => $massTimeEntry->getCostTypeCode(),
            'phrmtim_change_ind' => $massTimeEntry->getChangeIndicator(),
            'phrmtim_deemed_hrs' => $massTimeEntry->getDeemedHours(),
            'phrmtim_user_id' => $massTimeEntry->getUserId(),
            'phrmtim_data_origin' => $massTimeEntry->getDataOrigin()
        ]);

        return true;
    }

    public function readMassTimeEntry(string $year, string $payrollCode, string $payrollNumber, string $position, string $bannerId, string $suffix, string $earnCode,
                          string $shift, string $effectiveDate, string $beginDate)
    {
        $data = MassTimeEntryModel::where(
            'phrmtim_year', '=', $year)
            ->where('phrmtim_pict_code', $payrollCode)
            ->where('phrmtim_payno', $payrollNumber)
            ->where('phrmtim_id', $bannerId)
            ->whereRaw("TRUNC(phrmtim_begin_date) = TO_DATE( ? , 'MM-DD-YYYY')", [$beginDate])
            ->when($position, function($query) use($position){
                return $query->where('phrmtim_posn', $position);
            })
            ->when($suffix, function($query) use($suffix){
                return $query->where('phrmtim_suff', $suffix);
            })
            ->when($earnCode, function($query) use($earnCode){
                return $query->where('phrmtim_earn_code', $earnCode);
            })
            ->when($shift, function($query) use($shift){
                return $query->where('phrmtim_shift', $shift);
            })
            ->when($effectiveDate, function($query) use($effectiveDate){
                return $query->whereRaw("TRUNC(phrmtim_effective_date) = TO_DATE( ?, 'MM-DD-YYYY')", [$effectiveDate]);
            })
            ->get();

        $massTimeEntry = [];

        foreach ($data as $datum) {
            $massTimeEntryData['year'] = $datum['phrmtim_year'];
            $massTimeEntryData['payrollCode'] = $datum['phrmtim_pict_code'];
            $massTimeEntryData['payrollNumber'] = $datum['phrmtim_payno'];
            $massTimeEntryData['bannerId'] = $datum['phrmtim_id'];
            $massTimeEntryData['position'] = $datum['phrmtim_posn'];
            $massTimeEntryData['suffix'] = $datum['phrmtim_suff'];
            $massTimeEntryData['effectiveDate'] = $datum['phrmtim_effective_date'];
            $massTimeEntryData['earnCode'] = $datum['phrmtim_earn_code'];
            $massTimeEntryData['shift'] = $datum['phrmtim_shift'];
            $massTimeEntryData['beginDate'] = $datum['phrmtim_begin_date'];
            $massTimeEntryData['endDate'] = $datum['phrmtim_end_date'];
            $massTimeEntryData['specialRate'] = $datum['phrmtim_special_rate'];
            $massTimeEntryData['hours'] = $datum['phrmtim_hrs'];
            $massTimeEntryData['chartOfAccountsCode'] = $datum['phrmtim_coas_code'];
            $massTimeEntryData['accountIndexCode'] = $datum['phrmtim_acci_code'];
            $massTimeEntryData['fundCode'] = $datum['phrmtim_fund_code'];
            $massTimeEntryData['organizationCode'] = $datum['phrmtim_orgn_code'];
            $massTimeEntryData['accountCode'] = $datum['phrmtim_acct_code'];
            $massTimeEntryData['programCode'] = $datum['phrmtim_prog_code'];
            $massTimeEntryData['activityCode'] = $datum['phrmtim_actv_code'];
            $massTimeEntryData['locationCode'] = $datum['phrmtim_locn_code'];
            $massTimeEntryData['projectCode'] = $datum['phrmtim_proj_code'];
            $massTimeEntryData['costTypeCode'] = $datum['phrmtim_ctyp_code'];
            $massTimeEntryData['externalAccountCode'] = $datum['phrmtim_acct_code_external'];
            $massTimeEntryData['percent'] = $datum['phrmtim_percent'];
            $massTimeEntryData['activityDate'] = $datum['phrmtim_activity_date'];
            $massTimeEntryData['changeIndicator'] = $datum['phrmtim_change_ind'];
            $massTimeEntryData['deemedHours'] = $datum['phrmtim_deemed_hrs'];
            $massTimeEntryData['userId'] = $datum['phrmtim_user_id'];
            $massTimeEntryData['dataOrigin'] = $datum['phrmtim_data_origin'];
            $massTimeEntryData['surrogateId'] = $datum['phrmtim_surrogate_id'];
            $massTimeEntry[] = $massTimeEntryData;
        }

        return $massTimeEntry;
    }


}
