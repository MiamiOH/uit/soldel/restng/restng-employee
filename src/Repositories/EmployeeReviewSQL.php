<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-21
 * Time: 09:31
 */
namespace MiamiOH\RestngEmployee\Repositories;

use MiamiOH\RestngEmployee\EloquentModels\EmployeeReviewModel;

/**
 * Class EmployeeReviewSQL
 * @package MiamiOH\RestngEmployee\Repositories
 */
class EmployeeReviewSQL implements EmployeeReviewRepository
{
    /**
     * @var MUIDRepository
     */
    private $muidRepository;

    /**
     * EmployeeReviewSQL constructor.
     * @param MUIDRepository $muidRepository
     */
    public function __construct(MUIDRepository $muidRepository)
    {
        $this->muidRepository = $muidRepository;
    }

    /**
     * @param string $muid
     * @param string $fromCompletedDate
     * @param string $toCompletedDate
     * @return array
     */
    public function readEmployeeReview(
        string $muid,
        string $fromCompletedDate,
        string $toCompletedDate
    ) {
        $muid = $this->muidRepository->readMUID($muid);
        $employeePidm = $muid['pidm'];

        $data = EmployeeReviewModel::where(
            'perrevw_pidm',
            '=',
            $employeePidm
        )->when($fromCompletedDate, function ($query) use ($fromCompletedDate) {
            $query->whereRaw("TRUNC(perrevw_completed_date) >= TO_DATE( ? , 'MM-DD-YYYY')", [$fromCompletedDate]);
        })->when($toCompletedDate, function ($query) use ($toCompletedDate) {
            $query->whereRaw("TRUNC(perrevw_completed_date) <= TO_DATE( ? , 'MM-DD-YYYY')", [$toCompletedDate]);
        })
            ->where('perrevw_revt_complete', '=', 'Y')
            ->orderBy('perrevw_revt_date', 'desc')
            ->get();

        $employeeReview = [];

        foreach ($data as $datum) {
            $employeeReviewData['pidm'] = $datum['perrevw_pidm'];
            $employeeReviewData['reviewTypeCode'] = $datum['perrevw_revt_code'];
            $employeeReviewData['reviewTypeDate'] = $datum['perrevw_revt_date'];
            $employeeReviewData['reviewTypeComplete'] = $datum['perrevw_revt_complete'];
            $employeeReviewData['reviewTypeRating'] = $datum['perrevw_revt_rating'];
            $employeeReviewData['reviewerPidm'] = $datum['perrevw_reviewer_pidm'];
            $employeeReviewData['activityDate'] = $datum['perrevw_activity_date'];
            $employeeReviewData['comment'] = $datum['perrevw_comment'];
            $employeeReviewData['completedDate'] = $datum['perrevw_completed_date'];
            $employeeReviewData['userId'] = $datum['perrevw_user_id'];
            $employeeReviewData['dataOrigin'] = $datum['perrevw_data_origin'];
            $employeeReviewData['surrogateId'] = $datum['perrevw_surrogate_id'];
            $employeeReview[] = $employeeReviewData;
        }

        return $employeeReview;
    }
}