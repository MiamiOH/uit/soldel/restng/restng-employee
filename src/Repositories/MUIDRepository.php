<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-21
 * Time: 10:27
 */

namespace MiamiOH\RestngEmployee\Repositories;

interface MUIDRepository
{
    public function readMUIDs(array $muids): array;

    public function readMUID(string $muid): array;
}