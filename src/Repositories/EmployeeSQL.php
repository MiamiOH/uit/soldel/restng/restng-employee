<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/15/19
 * Time: 4:12 PM
 */

namespace MiamiOH\RestngEmployee\Repositories;

use MiamiOH\RestngEmployee\EloquentModels\EmployeeModel;
use MiamiOH\RestngEmployee\Services\EmployeeData;

class EmployeeSQL implements EmployeesRepository
{
    /**
     * @param EmployeeData $employee
     * @return array
     */
    public function createEmployee(EmployeeData $employee): bool
    {
        EmployeeModel::create([
            'pebempl_pidm' => $employee->getPidm(),
            'pebempl_empl_status' =>  $employee->getEmployeeStatus(),
            'pebempl_orgn_code_home' => $employee->getHomeOrgCode(),
            'pebempl_coas_code_home' => $employee->getHomeCoasCode(),
            'pebempl_orgn_code_dist' => $employee->getOrgCodeDistribution(),
            'pebempl_coas_code_dist' => $employee->getCoasCodeDistribution(),
            'pebempl_ecls_code' => $employee->getEmployeeClassCode(),
            'pebempl_lcat_code' => $employee->getLeaveCategoryCode(),
            'pebempl_bcat_code' => $employee->getBenefitCategoryCode(),
            'pebempl_first_hire_date' => $employee->getFirstHireDate(),
            'pebempl_current_hire_date' => $employee->getCurrentHireDate(),
            'pebempl_adj_service_date' => $employee->getServiceDate(),
            'pebempl_seniority_date' => $employee->getSeniorityDate(),
            'pebempl_activity_date' => $employee->getActivityDate(),
            'pebempl_trea_code' => $employee->getTerminatedReasonCode(),
            'pebempl_wkpr_code' => $employee->getWorkPeriodCode(),
            'pebempl_flsa_ind' => $employee->getFlsaIndicator(),
            'pebempl_internal_ft_pt_ind' => $employee->getInternalFullTimePartTimeIndicator(),
            'pebempl_dicd_code' => $employee->getDistrictCode(),
            'pebempl_ipeds_soft_money_ind' => $employee->getIpedsSoftMoneyIndicator(),
            'pebempl_first_work_date' => $employee->getFirstWorkDate(),
            'pebempl_last_work_date' => $employee->getLastWorkDate(),
            'pebempl_jbln_code' => $employee->getJobLocationCode(),
            'pebempl_camp_code' => $employee->getCampusCode(),
            'pebempl_coll_code' => $employee->getCollegeCode(),
            'pebempl_ew2_consent_ind' => $employee->getEw2ConsentIndicator(),
            'pebempl_ipeds_primary_function' => $employee->getIpedsPrimaryFunction(),
            'pebempl_ipeds_med_dental_ind' => $employee->getIpedsMedicalDentalIndicator(),
            'pebempl_etax_consent_ind' => $employee->getEtaxConsentIndicator(),
            'pebempl_new_hire_ind' => $employee->getNewHireIndicator(),
            'pebempl_1095tx_consent_ind' => $employee->getTax1095ConsentIndicator(),
            'pebempl_user_id' => $employee->getUserId(),
            'pebempl_data_origin' => $employee->getDataOrigin()
        ]);

        return true;
    }

    /**
     * @param EmployeeData $employee
     * @return array
     */
    public function updateEmployee(EmployeeData $employee): bool
    {
        $modifiedAttributes = $employee->getModifiedAttributes();

        $employeeModel = EmployeeModel::where('pebempl_pidm', $employee->getPidm())->first();

        if (isset($modifiedAttributes['employeeStatus'])) {
            $employeeModel->pebempl_empl_status = $employee->getEmployeeStatus();
        };

        if (isset($modifiedAttributes['homeOrgCode'])) {
            $employeeModel->pebempl_orgn_code_home = $employee->getHomeOrgCode();
        };

        if (isset($modifiedAttributes['homeCoasCode'])) {
            $employeeModel->pebempl_coas_code_home = $employee->getHomeCoasCode();
        };

        if (isset($modifiedAttributes['orgCodeDistribution'])) {
            $employeeModel->pebempl_orgn_code_dist = $employee->getOrgCodeDistribution();
        };

        if (isset($modifiedAttributes['coasCodeDistribution'])) {
            $employeeModel->pebempl_coas_code_dist = $employee->getCoasCodeDistribution();
        };

        if (isset($modifiedAttributes['employeeClassCode'])) {
            $employeeModel->pebempl_ecls_code = $employee->getEmployeeClassCode();
        };

        if (isset($modifiedAttributes['leaveCategoryCode'])) {
            $employeeModel->pebempl_lcat_code = $employee->getLeaveCategoryCode();
        };

        if (isset($modifiedAttributes['benefitCategoryCode'])) {
            $employeeModel->pebempl_bcat_code = $employee->getBenefitCategoryCode();
        };

        if (isset($modifiedAttributes['firstHireDate'])) {
            $employeeModel->pebempl_first_hire_date = $employee->getFirstHireDate();
        };

        if (isset($modifiedAttributes['currentHireDate'])) {
            $employeeModel->pebempl_current_hire_date = $employee->getCurrentHireDate();
        };

        if (isset($modifiedAttributes['serviceDate'])) {
            $employeeModel->pebempl_adj_service_date = $employee->getServiceDate();
        };

        if (isset($modifiedAttributes['seniorityDate'])) {
            $employeeModel->pebempl_seniority_date = $employee->getSeniorityDate();
        };

        if (isset($modifiedAttributes['terminatedReasonCode'])) {
            $employeeModel->pebempl_trea_code = $employee->getTerminatedReasonCode();
        };

        if (isset($modifiedAttributes['workPeriodCode'])) {
            $employeeModel->pebempl_wkpr_code = $employee->getWorkPeriodCode();
        };

        if (isset($modifiedAttributes['flsaIndicator'])) {
            $employeeModel->pebempl_flsa_ind = $employee->getFlsaIndicator();
        };

        if (isset($modifiedAttributes['internalFullTimePartTimeIndicator'])) {
            $employeeModel->pebempl_internal_ft_pt_ind = $employee->getInternalFullTimePartTimeIndicator();
        };

        if (isset($modifiedAttributes['districtCode'])) {
            $employeeModel->pebempl_dicd_code = $employee->getDistrictCode();
        };

        if (isset($modifiedAttributes['ipdesSoftMoneyIndicator'])) {
            $employeeModel->pebempl_ipeds_soft_money_ind = $employee->getIpedsSoftMoneyIndicator();
        };

        if (isset($modifiedAttributes['firstWorkDate'])) {
            $employeeModel->pebempl_first_work_date = $employee->getFirstWorkDate();
        };

        if (isset($modifiedAttributes['lastWorkDate'])) {
            $employeeModel->pebempl_last_work_date = $employee->getLastWorkDate();
        };

        if (isset($modifiedAttributes['jobLocationCode'])) {
            $employeeModel->pebempl_jbln_code = $employee->getJobLocationCode();
        };

        if (isset($modifiedAttributes['campusCode'])) {
            $employeeModel->pebempl_camp_code = $employee->getCampusCode();
        };

        if (isset($modifiedAttributes['collegeCode'])) {
            $employeeModel->pebempl_coll_code = $employee->getCollegeCode();
        };

        if (isset($modifiedAttributes['ew2ConsentIndicator'])) {
            $employeeModel->pebempl_ew2_consent_ind = $employee->getEw2ConsentIndicator();
        };

        if (isset($modifiedAttributes['ipedsPrimaryFunction'])) {
            $employeeModel->pebempl_ipeds_primary_function = $employee->getIpedsPrimaryFunction();
        };

        if (isset($modifiedAttributes['ipdesMedicalDentalIndicator'])) {
            $employeeModel->pebempl_ipeds_med_dental_ind = $employee->getIpedsMedicalDentalIndicator();
        };

        if (isset($modifiedAttributes['etaxConsentIndicator'])) {
            $employeeModel->pebempl_etax_consent_ind = $employee->getEtaxConsentIndicator();
        };

        if (isset($modifiedAttributes['newHireIndicator'])) {
            $employeeModel->pebempl_new_hire_ind = $employee->getNewHireIndicator();
        };

        if (isset($modifiedAttributes['tax1095ConsentIndicator'])) {
            $employeeModel->pebempl_1095tx_consent_ind = $employee->getTax1095ConsentIndicator();
        };

        $employeeModel->pebempl_activity_date = $employee->getActivityDate();
        $employeeModel->pebempl_user_id = strtoupper($employee->getUserId());
        $employeeModel->pebempl_data_origin = $employee->getDataOrigin();

        $employeeModel->save();

        return true;

    }

}
