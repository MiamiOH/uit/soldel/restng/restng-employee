<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-21
 * Time: 09:30
 */

namespace MiamiOH\RestngEmployee\Repositories;

interface EmployeeReviewRepository
{
    public function readEmployeeReview(
        string $muid,
        string $fromCompletedDate,
        string $toCompletedDate
    );
}