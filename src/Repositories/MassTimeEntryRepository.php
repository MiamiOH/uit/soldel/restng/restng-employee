<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-29
 * Time: 14:48
 */

namespace MiamiOH\RestngEmployee\Repositories;

use MiamiOH\RestngEmployee\Objects\MassTimeEntry;

interface MassTimeEntryRepository
{
    public function createMassTimeEntry(MassTimeEntry $massTimeEntry);

    public function readMassTimeEntry(string $year, string $payrollCode, string $payrollNumber, string $position, string $bannerId, string $suffix, string $earnCode,
                                       string $shift, string $effectiveDate, string $beginDate);
}