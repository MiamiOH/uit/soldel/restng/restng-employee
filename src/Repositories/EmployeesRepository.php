<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/15/19
 * Time: 4:11 PM
 */

namespace MiamiOH\RestngEmployee\Repositories;


use MiamiOH\RestngEmployee\Services\EmployeeData;

interface EmployeesRepository
{
    public function createEmployee(EmployeeData $employeeData);

    public function updateEmployee(EmployeeData $employeeData);
}