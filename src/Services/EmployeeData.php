<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 1/16/19
 * Time: 9:41 AM
 */

namespace MiamiOH\RestngEmployee\Services;

use Exception;
use MiamiOH\RESTng\Util\User;

/**
 * Class EmployeeData
 * @package MiamiOH\RestngEmployee\Services
 */
class EmployeeData
{
    /**
     *
     */
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $pidm = '';

    /**
     * @var string
     */
    private $employeeStatus = '';

    /**
     * @var string
     */
    private $homeOrgCode = '';


    /**
     * @var string
     */
    private $homeCoasCode = '';


    /**
     * @var string
     */
    private $orgCodeDistribution = '';


    /**
     * @var string
     */
    private $coasCodeDistribution = '';


    /**
     * @var string
     */
    private $employeeClassCode = '';


    /**
     * @var string
     */
    private $leaveCategoryCode = '';


    /**
     * @var string
     */
    private $benefitCategoryCode = '';


    /**
     * @var \DateTime
     */
    private $firstHireDate = null;


    /**
     * @var \DateTime
     */
    private $currentHireDate = null;


    /**
     * @var \DateTime
     */
    private $serviceDate = null;


    /**
     * @var \DateTime
     */
    private $seniorityDate = null;


    /**
     * @var string
     */
    private $terminatedReasonCode = '';


    /**
     * @var string
     */
    private $workPeriodCode = '';


    /**
     * @var string
     */
    private $flsaIndicator = '';


    /**
     * @var string
     */
    private $internalFullTimePartTimeIndicator = '';


    /**
     * @var string
     */
    private $districtCode = '';


    /**
     * @var \DateTime
     */
    private $firstWorkDate = null;


    /**
     * @var \DateTime
     */
    private $lastWorkDate = null;


    /**
     * @var string
     */
    private $jobLocationCode = '';


    /**
     * @var string
     */
    private $campusCode = '';


    /**
     * @var string
     */
    private $collegeCode = '';


    /**
     * @var string
     */
    private $ipedsSoftMoneyIndicator = '';


    /**
     * @var string
     */
    private $ipedsMedicalDentalIndicator = '';


    /**
     * @var string
     */
    private $ipedsPrimaryFunction = '';


    /**
     * @var string
     */
    private $ew2ConsentIndicator = '';


    /**
     * @var string
     */
    private $etaxConsentIndicator = '';


    /**
     * @var string
     */
    private $newHireIndicator = '';


    /**
     * @var string
     */
    private $tax1095ConsentIndicator = '';


    /**
     * @var string
     */
    private $userId = '';


    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @param $pidm
     * @return EmployeeData
     */
    public function setPidm(string $pidm): EmployeeData
    {
        $this->pidm = $pidm;
        return $this;
    }

    /**
     * @param $employeeStatus
     * @return EmployeeData
     */
    public function setEmployeeStatus(string $employeeStatus): EmployeeData
    {
        $this->employeeStatus = $employeeStatus;
        return $this;
    }


    /**
     * @param $homeOrgCode
     * @return EmployeeData
     */
    public function setHomeOrgcode(string $homeOrgCode): EmployeeData
    {
        $this->homeOrgCode = $homeOrgCode;
        return $this;
    }


    /**
     * @param $homeCoasCode
     * @return EmployeeData
     */
    public function setHomeCoascode(string $homeCoasCode): EmployeeData
    {
        $this->homeCoasCode= $homeCoasCode;
        return $this;
    }


    /**
     * @param $orgCodeDist
     * @return EmployeeData
     */
    public function setOrgCodeDistribution(string $orgCodeDist): EmployeeData
    {
        $this->orgCodeDistribution = $orgCodeDist;
        return $this;
    }


    /**
     * @param $coasCodeDist
     * @return EmployeeData
     */
    public function setCoasCodeDistribution(string $coasCodeDist): EmployeeData
    {
        $this->coasCodeDistribution = $coasCodeDist;
        return $this;
    }


    /**
     * @param $employeeClassCode
     * @return EmployeeData
     */
    public function setEmployeeClassCode(string $employeeClassCode): EmployeeData
    {
        $this->employeeClassCode = $employeeClassCode;
        return $this;
    }


    /**
     * @param $leaveCategoryCode
     * @return EmployeeData
     */
    public function setLeaveCategoryCode(string $leaveCategoryCode): EmployeeData
    {
        $this->leaveCategoryCode = $leaveCategoryCode;
        return $this;
    }


    /**
     * @param $benefitCategoryCode
     * @return EmployeeData
     */
    public function setBenefitCategoryCode(string $benefitCategoryCode): EmployeeData
    {
        $this->benefitCategoryCode = $benefitCategoryCode;
        return $this;
    }

    /**
     * @param $firstHireDate
     * @return EmployeeData
     */
    public function setFirstHireDate(string $firstHireDate): EmployeeData
    {
        $this->firstHireDate = new \DateTime($firstHireDate);
        return $this;
    }

    /**
     * @param $currentHireDate
     * @return EmployeeData
     */
    public function setCurrentHireDate(string $currentHireDate): EmployeeData
    {
        $this->currentHireDate = new \DateTime($currentHireDate);
        return $this;
    }

    /**
     * @param $serviceDate
     * @return EmployeeData
     */
    public function setServiceDate(string $serviceDate): EmployeeData
    {
        $this->serviceDate = new \DateTime($serviceDate);
        return $this;
    }

    /**
     * @param $seniorityDate
     * @return EmployeeData
     */
    public function setSeniorityDate($seniorityDate): EmployeeData
    {
        $this->seniorityDate = new \DateTime($seniorityDate);
        return $this;
    }

    /**
     * @param $terminatedReasonCode
     * @return EmployeeData
     */
    public function setTerminatedReasonCode(string $terminatedReasonCode): EmployeeData
    {
        $this->terminatedReasonCode = $terminatedReasonCode;
        return $this;
    }

    /**
     * @param $workPeriodCode
     * @return EmployeeData
     */
    public function setWorkPeriodCode(string $workPeriodCode): EmployeeData
    {
        $this->workPeriodCode = $workPeriodCode;
        return $this;
    }

    /**
     * @param $flsaIndicator
     * @return EmployeeData
     */
    public function setFlsaindicator(string $flsaIndicator): EmployeeData
    {
        $this->flsaIndicator = $flsaIndicator;
        return $this;
    }

    /**
     * @param $internalFtPtIndicator
     * @return EmployeeData
     */
    public function setInternalFullTimePartTimeIndicator(string $internalFtPtIndicator): EmployeeData
    {
        $this->internalFullTimePartTimeIndicator = $internalFtPtIndicator;
        return $this;
    }


    /**
     * @param $districtCode
     * @return EmployeeData
     */
    public function setDistrictCode(string $districtCode): EmployeeData
    {
        $this->districtCode = $districtCode;
        return $this;
    }


    /**
     * @param string $firstWorkDate
     * @return EmployeeData
     * @throws Exception
     */
    public function setFirstWorkDate(string $firstWorkDate): EmployeeData
    {
        if(empty($firstWorkDate)){
            $this->firstWorkDate = null;
        } else {
            $this->firstWorkDate = new \DateTime($firstWorkDate);
        }

        return $this;
    }


    /**
     * @param string $lastWorkDate
     * @return EmployeeData
     */
    public function setLastWorkDate(string $lastWorkDate): EmployeeData
    {
        if (empty($lastWorkDate)) {
            $this->lastWorkDate = null;
        } else {
            $this->lastWorkDate = new \DateTime($lastWorkDate);
        }

        return $this;
    }


    /**
     * @param $jobLocationCode
     * @return EmployeeData
     */
    public function setJobLocationCode(string $jobLocationCode): EmployeeData
    {
        $this->jobLocationCode = $jobLocationCode;
        return $this;
    }


    /**
     * @param $campusCode
     * @return EmployeeData
     */
    public function setCampusCode(string $campusCode): EmployeeData
    {
        $this->campusCode = $campusCode;
        return $this;
    }


    /**
     * @param $collegeCode
     * @return EmployeeData
     */
    public function setCollegeCode(string $collegeCode): EmployeeData
    {
        $this->collegeCode = $collegeCode;
        return $this;
    }


    /**
     * @param $ipedsSoftMoneyIndicator
     * @return EmployeeData
     */
    public function setIpedsSoftMoneyIndicator(string $ipedsSoftMoneyIndicator): EmployeeData
    {
        $this->ipedsSoftMoneyIndicator = $ipedsSoftMoneyIndicator;
        return $this;
    }


    /**
     * @param $ipedsMedicalDentalIndicator
     * @return EmployeeData
     */
    public function setIpedsMedicalDentalIndicator(string $ipedsMedicalDentalIndicator): EmployeeData
    {
        $this->ipedsMedicalDentalIndicator = $ipedsMedicalDentalIndicator;
        return $this;
    }


    /**
     * @param $ipedsPrimaryFunction
     * @return EmployeeData
     */
    public function setIpedsPrimaryFunction(string $ipedsPrimaryFunction): EmployeeData
    {
        $this->ipedsPrimaryFunction = $ipedsPrimaryFunction;
        return $this;
    }


    /**
     * @param $ew2ConsentIndicator
     * @return EmployeeData
     */
    public function setEw2ConsentIndicator(string $ew2ConsentIndicator): EmployeeData
    {
        $this->ew2ConsentIndicator = $ew2ConsentIndicator;
        return $this;
    }


    /**
     * @param $etaxConsentIndicator
     * @return EmployeeData
     */
    public function setEtaxConsentIndicator(string $etaxConsentIndicator): EmployeeData
    {
        $this->etaxConsentIndicator = $etaxConsentIndicator;
        return $this;
    }


    /**
     * @param $newHireIndicator
     * @return EmployeeData
     */
    public function setNewHireIndicator(string $newHireIndicator): EmployeeData
    {
        $this->newHireIndicator = $newHireIndicator;
        return $this;
    }


    /**
     * @param $tax1095ConsentIndicator
     * @return EmployeeData
     */
    public function setTax1095ConsentIndicator(string $tax1095ConsentIndicator): EmployeeData
    {
        $this->tax1095ConsentIndicator = $tax1095ConsentIndicator;
        return $this;
    }


    /**
     * @param $userId
     * @return EmployeeData
     */
    public function setUserId(string $userId): EmployeeData
    {
        $this->userId = $userId;
        return $this;
    }


    /**
     * @param $dataOrigin
     * @return EmployeeData
     */
    public function setDataOrigin(string $dataOrigin): EmployeeData
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }


    /**
     * @return string
     */
    public function getPidm(): string
    {
        return $this->pidm;
    }


    /**
     * @return string
     */
    public function getEmployeeStatus(): string
    {
        return $this->employeeStatus;
    }


    /**
     * @return string
     */
    public function getHomeOrgCode(): string
    {
        return strtoupper($this->homeOrgCode);
    }


    /**
     * @return string
     */
    public function getHomeCoasCode(): string
    {
        return strtoupper($this->homeCoasCode);
    }


    /**
     * @return string
     */
    public function getOrgCodeDistribution(): string
    {
        return strtoupper($this->orgCodeDistribution);
    }


    /**
     * @return string
     */
    public function getCoasCodeDistribution(): string
    {
        return strtoupper($this->coasCodeDistribution);
    }


    /**
     * @return string
     */
    public function getEmployeeClassCode(): string
    {
        return strtoupper($this->employeeClassCode);
    }


    /**
     * @return string
     */
    public function getLeaveCategoryCode(): string
    {
        return strtoupper($this->leaveCategoryCode);
    }


    /**
     * @return string
     */
    public function getBenefitCategoryCode(): string
    {
        return strtoupper($this->benefitCategoryCode);
    }


    /**
     * @param string $format
     * @return string
     */
    public function getFirstHireDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if(empty($this->firstHireDate)){
            return '';
        }
        return $this->firstHireDate->format($format);
    }


    /**
     * @param string $format
     * @return string
     */
    public function getCurrentHireDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->currentHireDate)) {
            return '';
        }
        return $this->currentHireDate->format($format);
    }


    /**
     * @param string $format
     * @return string
     */
    public function getServiceDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if(empty($this->serviceDate)){
            return '';
        }
        return $this->serviceDate->format($format);
    }


    /**
     * @param string $format
     * @return string
     */
    public function getSeniorityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if(empty($this->seniorityDate)){
            return '';
        }
        return $this->seniorityDate->format($format);
    }


    /**
     * @param string $format
     * @return string
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }


    /**
     * @return string
     */
    public function getTerminatedReasonCode(): string
    {
        return strtoupper($this->terminatedReasonCode);
    }


    /**
     * @return string
     */
    public function getWorkPeriodCode(): string
    {
        return strtoupper($this->workPeriodCode);
    }


    /**
     * @return string
     */
    public function getFlsaIndicator(): string
    {
        return $this->flsaIndicator;
    }


    /**
     * @return string
     */
    public function getInternalFullTimePartTimeIndicator(): string
    {
        return $this->internalFullTimePartTimeIndicator;
    }


    /**
     * @return string
     */
    public function getDistrictCode(): string
    {
        return strtoupper($this->districtCode);
    }


    /**
     * @return string
     */
    public function getIpedsSoftMoneyIndicator(): string
    {
        return $this->ipedsSoftMoneyIndicator;
    }

    /**
     * @return string
     */
    public function getFirstWorkDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if(empty($this->firstWorkDate)){
            return '';
        }

        return $this->firstWorkDate->format($format);
    }

    /**
     * @return string
     */
    public function getLastWorkDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if(empty($this->lastWorkDate)){
            return '';
        }

        return $this->lastWorkDate->format($format);
    }


    /**
     * @return string
     */
    public function getJobLocationCode(): string
    {
        return strtoupper($this->jobLocationCode);
    }


    /**
     * @return string
     */
    public function getCampusCode(): string
    {
        return strtoupper($this->campusCode);
    }


    /**
     * @return string
     */
    public function getCollegeCode(): string
    {
        return strtoupper($this->collegeCode);
    }


    /**
     * @return string
     */
    public function getEw2ConsentIndicator(): string
    {
        return $this->ew2ConsentIndicator;
    }


    /**
     * @return string
     */
    public function getIpedsPrimaryFunction(): string
    {
        return $this->ipedsPrimaryFunction;
    }

    /**
     * @return string
     */
    public function getIpedsMedicalDentalIndicator(): string
    {
        return $this->ipedsMedicalDentalIndicator;
    }


    /**
     * @return string
     */
    public function getEtaxConsentIndicator(): string
    {
        return $this->etaxConsentIndicator;
    }


    /**
     * @return string
     */
    public function getNewHireIndicator(): string
    {
        return $this->newHireIndicator;
    }


    /**
     * @return string
     */
    public function getTax1095ConsentIndicator(): string
    {
        return $this->tax1095ConsentIndicator;
    }


    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId ;
    }


    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin = 'WebService';
    }

    private $modifiedAttributes = [];

    /**
     * @return array
     */
    public function getModifiedAttributes(): array
    {
        return $this->modifiedAttributes;
    }

    /**
     * @param string $attributes
     */
    public function addModifiedAttributes(string $attributes): void
    {
        $this->modifiedAttributes[$attributes] = true;
    }


    /**
     * @param array $employeeData
     * @return self
     * @throws Exception
     */
    public static function fromArray(array $employeeData): self
    {
        $employee = new self();

        try {
            foreach ($employeeData as $key => $val) {
                $employee->{'set' . ucfirst($key)}($val);
                $employee->addModifiedAttributes($key);
            }
        } catch (Exception $e) {
            throw new Exception("Invalid key for Position: $key" . $e->getMessage());
        }

        return $employee;
    }


}