<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-31
 * Time: 10:28
 */

namespace MiamiOH\RestngEmployee\Services\MassTimeEntry;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\RestngEmployee\Repositories\MassTimeEntryRepository;

class Get
{
    private $rules = [
        'year' => ['required', 'max:4'],
        'payrollCode' => ['required', 'max:2'],
        'payrollNumber' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'bannerId' => ['required', 'max:9'],
        'beginDate' => ['required', 'date'],
        'effectiveDate' => ['date'],
        'position' => ['max:6'],
        'suffix' => ['max:2'],
        'earnCode' => ['max:3'],
        'shift' => ['max:1'],
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param MassTimeEntryRepository $repository
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getMassTimeEntry(
        Request $request,
        Response $response,
        MassTimeEntryRepository $repository
    )
    {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $year = $options['year'] ?? '';

        $payCode = $options['payrollCode'] ?? '';

        $payNumber = $options['payrollNumber'] ?? '';

        $earnCode = $options['earnCode'] ?? '';

        $suffix = $options['suffix'] ?? '';

        $position = $options['position'] ?? '';

        $bannerId = $options['bannerId'] ?? '';

        $shift = $options['shift'] ?? '';

        $beginDate = $options['beginDate'] ?? '';

        $effectiveDate = $options['effectiveDate'] ?? '';

        //Validate input parameters
        $year = trim($year);
        $payCode = trim($payCode);
        $payNumber = trim($payNumber);
        $earnCode = trim($earnCode);
        $suffix = trim($suffix);
        $position = trim($position);
        $bannerId = trim($bannerId);
        $shift = trim($shift);
        $beginDate = trim($beginDate);
        $effectiveDate = trim($effectiveDate);

        $validator = RESTngValidatorFactory::make(
            [
                'year' => $year,
                'payrollCode' => $payCode,
                'payrollNumber' => $payNumber,
                'earnCode' => $earnCode,
                'suffix' => $suffix,
                'position' => $position,
                'bannerId' => $bannerId,
                'shift' => $shift,
                'beginDate' => $beginDate,
                'effectiveDate' => $effectiveDate
            ],
            $this->rules
        );


        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }

        $payload = $repository->readMassTimeEntry($year, $payCode, $payNumber, $position, $bannerId, $suffix, $earnCode, $shift, $effectiveDate, $beginDate);

        if (empty($payload)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $response->setTotalObjects(count($payload));
        $response->setPayload($payload);
        $response->setStatus($status);

        return $response;

    }

}
