<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 05/31/19
 * Time: 16:05
 */

namespace MiamiOH\RestngEmployee\Services\MassTimeEntry;

use MiamiOH\RESTng\Util\User;
use MiamiOH\RestngEmployee\Objects\MassTimeEntry;
use MiamiOH\RestngEmployee\Repositories\MassTimeEntryRepository;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;

class Post
{
    /**
     * @param Request $request
     * @param Response $response
     * @param User $user
     * @param MassTimeEntryRepository $repository
     * @return Response
     * @throws \Exception
     */
    public function postSingle(
        Request $request,
        Response $response,
        User $user,
        MassTimeEntryRepository $repository
    ): Response
    {
        $status = \MiamiOH\RESTng\App::API_CREATED;

        $data = $request->getData();

        if (empty($data)) {
            $payload['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayload($payload);
            $response->setStatus($status);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WEB SERVICE';

        try {
            $massTimeEntry = MassTimeEntry::fromArray($data);
            $repository->createMassTimeEntry($massTimeEntry);
        } catch (\Exception $e) {
            $payload['errors'][] = $e->getMessage();
            $response->setPayload($payload);
            if ($e instanceof \InvalidArgumentException) {
                $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            } else {
                $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            }
            return $response;
        }

// DONE
        $response->setPayload(['1 record created.']);
        $response->setStatus($status);
        return $response;
    }
}
