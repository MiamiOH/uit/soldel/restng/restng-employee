<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 05/31/19
 * Time: 10:05 AM
 */

namespace MiamiOH\RestngEmployee\Services\MassTimeEntry;

use MiamiOH\RestngEmployee\Repositories\MassTimeEntryRepository;
use MiamiOH\RestngEmployee\Repositories\MassTimeEntrySQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var MassTimeEntryRepository
     */
    protected $repository = null;

    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $this->repository = new MassTimeEntrySQL();
    }

    /**
     * @throws \Exception
     */
    public function postSingle()
    {
        $this->getDependencies();

        $postService = new Post();

        $response = $postService->postSingle(
            $this->request,
            $this->response,
            $this->user,
            $this->repository
        );

        return $response;
    }

    /**
     * @throws \Exception
     */
    public function getMassTimeEntry()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getMassTimeEntry(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }

}
