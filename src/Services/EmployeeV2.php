<?php

namespace MiamiOH\RestngEmployee\Services;

use MiamiOH\RestngEmployee\Repositories\EmployeeSQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\RESTng\Legacy\DataSource;

class EmployeeV2 extends \MiamiOH\RESTng\Service
{
    private $dbDataSourceName = 'MUWS_GEN_PROD';

    private $dbh;

    public function setDatabase($database)
    {
        $this->dbh = $database->getHandle($this->dbDataSourceName);
    }

    public function setBannerUtil($bannerUtil)
    {
        /** @var \MiamiOH\RESTng\Service\Extension\BannerUtil
         * $bannerUtil
         */
        $this->bannerUtil = $bannerUtil;
    }

    private $rules = [
        'employeeStatus' => 'required|max:1',
        'homeOrgCode' => 'required|max:6',
        'homeCoasCode' => 'max:1',
        'orgCodeDistribution' => 'required|max:6',
        'coasCodeDistribution' => 'max:1',
        'employeeClassCode' => 'required|max:2',
        'leaveCategoryCode' => 'required|max:2',
        'benefitCategoryCode' => 'required|max:2',
        'firstHireDate' => 'required|date',
        'currentHireDate' => 'required|date',
        'serviceDate' => 'required|date',
        'seniorityDate' => 'required|date',
        'terminatedReasonCode' => 'max:2',
        'workPeriodCode' => 'max:4',
        'flsaIndicator' => 'required|max:1',
        'internalFullTimePartTimeIndicator' => 'required|max:1',
        'districtCode' => 'max:3',
        'ipedsSoftMoneyIndicator' => 'required|max:1',
        'jobLocationCode' => 'max:6',
        'campusCode' => 'max:3',
        'collegeCode' => 'max:2',
        'ew2ConsentIndicator' => 'required|max:1',
        'ipedsPrimaryFunction' => 'required|max:3',
        'ipedsMedicalDentalIndicator' => 'required|max:1',
        'etaxConsentIndicator' => 'required|max:1',
        'newHireIndicator' => 'required|max:1',
        'tax1095ConsentIndicator' => 'required|max:1',
        'userId' => 'max:30',
        'dataOrigin' => 'max:30',
    ];

    private $updateRules = [
        'employeeStatus' => 'max:1',
        'homeOrgCode' => 'max:6',
        'homeCoasCode' => 'max:1',
        'orgCodeDistribution' => 'max:6',
        'coasCodeDistribution' => 'max:1',
        'employeeClassCode' => 'max:2',
        'leaveCategoryCode' => 'max:2',
        'benefitCategoryCode' => 'max:2',
        'firstHireDate' => 'date',
        'currentHireDate' => 'date',
        'serviceDate' => 'date',
        'seniorityDate' => 'date',
        'terminatedReasonCode' => 'max:2',
        'workPeriodCode' => 'max:4',
        'flsaIndicator' => 'max:1',
        'internalFullTimePartTimeIndicator' => 'max:1',
        'districtCode' => 'max:3',
        'ipedsSoftMoneyIndicator' => 'max:1',
        'jobLocationCode' => 'max:6',
        'campusCode' => 'max:3',
        'collegeCode' => 'max:2',
        'ew2ConsentIndicator' => 'max:1',
        'ipedsPrimaryFunction' => 'max:3',
        'ipedsMedicalDentalIndicator' => 'max:1',
        'etaxConsentIndicator' => 'max:1',
        'newHireIndicator' => 'max:1',
        'tax1095ConsentIndicator' => 'max:1',
        'userId' => 'max:30',
        'dataOrigin' => 'max:30',
    ];

    private $messages = [
        'unique' => 'The :attribute has already been taken.',
        'required' => 'The :attribute field is required.',
        'max' => 'Maximum length exceeded for :attribute.',
    ];


    public function getEmployees()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        //paging enforced
        $offset = $request->getOffset();
        $limit = $request->getLimit();


        $where = array();

        if (isset($options['uniqueId'])) {
            $uids = $options['uniqueId'];
            $this->validateInput($uids, $this->getPattern("uniqueId"), "Invalid Unique ID.");
            // $where[] = "szbuniq_unique_id in (".strtoupper(implode(",",$uids)).")";
            $where[] = "szbuniq_unique_id in (" . $this->buildInString($uids, true) . ")";

        }

        if (isset($options['pidm'])) {
            $pidms = $options['pidm'];
            $this->validateInput($pidms, $this->getPattern("pidm"), "Invalid pidm.");
            $where[] = "pebempl_pidm in (" . $this->buildInString($pidms, false) . ")";

        }
        if (isset($options['fullTimeCode'])) {
            $ftCodes = $options['fullTimeCode'];
            $this->validateInput($ftCodes, '/^[a-zA-Z]{2}$/', "Invalid Full-time Code.");
            $where[] = "ref_ft.ft_pt_group_code in (" . $this->buildInString($ftCodes, true) . ")";

        }

        //Grad Level Filter
        if (isset($options['gradLevel'])) {
            $glCodes = array_unique(array_map('strtoupper', $options['gradLevel']));
            $this->validateInput($glCodes, '/^[A-Z]{2,}$/', "Invalid Grad Level.");
            $where[] = '(SELECT pprcert_cert_code FROM pprcert
                           WHERE pprcert_pidm = pebempl_pidm AND ROWNUM=1) in (' . $this->buildInString($glCodes,
                    true) . ")";
        }

        //Tenure Code Filter
        if (isset($options['tenureCode'])) {
            $tenureCodes = array_unique(array_map('strtoupper', $options['tenureCode']));
            $this->validateInput($tenureCodes, '/^[A-Z]{1,}$/', 'Invalid Tenure Code.');
            $where[] = '(SELECT perappt_tenure_code FROM perappt a
                            WHERE a.perappt_pidm =  pebempl_pidm
                            AND a.perappt_action_date =  (select max(b.perappt_action_date) from perappt b
                                                          where a.perappt_pidm = b.perappt_pidm)) in (' . $this->buildInString($tenureCodes,
                    true) . ")";
        }

        //Rank Code Filter
        if (isset($options['rankCode'])) {
            $rankCodes = array_unique(array_map('strtoupper', $options['rankCode']));
            $this->validateInput($rankCodes, '/^[A-Z]{2,}$/', "Invalid Rank Code.");
            $where[] = '(FZ_GET_FACULTY_RANK_CODE(pebempl_pidm) in (' . $this->buildInString($rankCodes, true) .
                ") OR (FZ_GET_LIBRARIAN_RANK_CODE(pebempl_pidm) in (" . $this->buildInString($rankCodes, true) . ")))";
        }

        //Home Org Code Filter
        if (isset($options['homeOrgCode'])) {
            $homeOrgCodes = array_unique(array_map('strtoupper', $options['homeOrgCode']));
            $this->validateInput($homeOrgCodes, '/^[A-Z0-9]{2,}$/', "Invalid Home Org Code.");
            //$where[] = "(a.nbrjobs_orgn_code_ts in (" . $this->buildInString($homeOrgCodes, true) . "))";
            $where[] = "(PEBEMPL_ORGN_CODE_HOME in (" . $this->buildInString($homeOrgCodes, true) . "))";

        }

        //Standardized Division Code Filter
        if (isset($options['standardizedDivisionCode'])) {
            $stdDivCode = array_unique(array_map('strtoupper', $options['standardizedDivisionCode']));
            $this->validateInput($stdDivCode, '/^[A-Z0-9]{1,}$/', "Invalid Standardized Division Code.");
            $where[] = "(ia_ref_organization_department_division.STANDARDIZED_DIVISION_CD in (" . $this->buildInString($stdDivCode, true) . "))";
        }

        //Standardized Department Code Filter
        if (isset($options['standardizedDepartmentCode'])) {
            $stdDeptCode = array_unique(array_map('strtoupper', $options['standardizedDepartmentCode']));
            $this->validateInput($stdDeptCode, '/^[A-Z0-9]{1,}$/', "Invalid Standardized Department Code.");
            $where[] = "(ia_ref_organization_department_division.STANDARDIZED_DEPARTMENT_CD in (" . $this->buildInString($stdDeptCode, true) . "))";
        }

        //Professional Librarian Filter
        if (isset($options['professionalLibrarian'])) {
            $librarian = $options['professionalLibrarian'];
            $this->validateInput($librarian, '/^(TRUE|FALSE)$/i', "Invalid professionalLibrarian filter value.");
            if (strtoupper($librarian) === 'TRUE') {
                $where[] = "FZ_GET_LIBRARIAN_RANK_CODE(pebempl_pidm) is not null";
            } else {
                $where[] = "FZ_GET_LIBRARIAN_RANK_CODE(pebempl_pidm) is null";
            }

        }

        //Dual Appointment Code Filter
        if (isset($options['dualAppointmentCode'])) {
            $stdDeptCode = array_unique(array_map('strtoupper', $options['dualAppointmentCode']));
            $this->validateInput($stdDeptCode, '/^[A-Z0-9]{1,}$/', "Invalid Dual Appointment Code.");
            $where[] = "(dualCodes.perfacc_facc_code in (" . $this->buildInString($stdDeptCode, true) . "))";
        }

        //set the default filter value for standardized department code and name
        $deptWhere = "ia_ref_organization_department_division.CAMPUS_GROUP_CD = case when f_orgn_hier2_fnc('C',pebempl_orgn_code_home,2) = 'Hamilton Campus' then 'H'
                         when f_orgn_hier2_fnc('C',pebempl_orgn_code_home,2) = 'Middletown Campus' then 'M'
                        else 'O' end";
        //change the filter value if a valide option value of TRUE is passed in
        if (isset($options['traditionalDepartment'])) {
            $traditionalDepartment = $options['traditionalDepartment'];
            $this->validateInput($traditionalDepartment, '/^(TRUE|FALSE)$/i', "Invalid traditionalDepartment filter value.");
            if (strtoupper($traditionalDepartment) === 'TRUE') {
                $deptWhere = "ia_ref_organization_department_division.CAMPUS_GROUP_CD='O'";
            }
        }

        if(isset($options['classificationCode'])){
            $classificationCodes = array_unique(array_map('strtoupper', $options['classificationCode']));
            $this->validateInput($classificationCodes, '/^[A-Z0-9]{2}$/', "Invalid Classification Code.");
            if(isset($options['includeOrExcludeClassCode']) && $options['includeOrExcludeClassCode'] == 'exclude' ) {
                $where[] = "(pebempl_ecls_code not in (" .$this->buildInString($classificationCodes, true). "))";
            } else {
                $where[] = "(pebempl_ecls_code in (" .$this->buildInString($classificationCodes, true). "))";
            }
        }

        $whereStr = "";
        if (sizeof($where) > 0) {
            $whereStr = " AND " . implode(" AND ", $where);
        }

        $payload = array();


        $query = "
        SELECT *
        FROM (SELECT a.*, ROWNUM rnum
            FROM ("
            . $this->getEmployeeQuery($whereStr, $deptWhere) .
            " ORDER BY pebempl_pidm) a
            WHERE  rownum <=" . ($limit + $offset - 1) . ")
        WHERE  rnum >= " . $offset;


        $results = $this->dbh->queryall_array($query);


        foreach ($results as $row) {
            // while($row = $sql->fetchrow_assoc()) {
            $payload[] = $this->buildRecord($row);
        }

        //get the total object number
        $query = "select count(pebempl_pidm) as total " . $this->getQueryBody($deptWhere) . $whereStr;

        $results = $this->dbh->queryall_array($query);

        if ($results[0]) {
            $totalObjects = $results[0]['total'];
        }


        //check whether there are sub objects request and call resources to get additional payload
        $subObjects = $request->getSubObjects();

        if (isset($subObjects) && sizeof($subObjects) > 0) {
            $payload_all = array();

            $payload_all = $this->addSubObjects($payload, $subObjects, false, $limit);
            $response->setPayload($payload_all);

        } else {
            $response->setPayload($payload);
        }

        $response->setTotalObjects($totalObjects);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }

    public function getOneEmployee()
    {

        $payload = array();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $options = $request->getOptions();


        $eid = $request->getResourceParam('muid');

        //validation
        switch ($request->getResourceParamKey('muid')) {
            case 'uniqueId':
                $this->validateInput($eid, $this->getPattern("uniqueId"), "Invalid unique ID.");
                break;
            case 'pidm':
                $this->validateInput($eid, $this->getPattern("pidm"), "Invalid pidm.");
                break;
        }

        try {
            $person = $this->bannerUtil->getId($request->getResourceParamKey('muid'),
                $request->getResourceParam('muid'));
            // $uid = $person->getUniqueId();
            $pidm = $person->getPidm();


        } catch (\Exception $e) {
            throw new \MiamiOH\RESTng\Exception\BadRequest($request->getResourceParamKey('muid') . " does not exist.");
        }

        $where = array("pebempl_pidm =$pidm");

        //set the default filter value for standardized department code and name
        $deptWhere = "ia_ref_organization_department_division.CAMPUS_GROUP_CD = case when f_orgn_hier2_fnc('C',pebempl_orgn_code_home,2) = 'Hamilton Campus' then 'H'
                         when f_orgn_hier2_fnc('C',pebempl_orgn_code_home,2) = 'Middletown Campus' then 'M'
                        else 'O' end";
        //change the filter value if a valide option value of TRUE is passed in
        if (isset($options['traditionalDepartment'])) {
            $traditionalDepartment = $options['traditionalDepartment'];
            $this->validateInput($traditionalDepartment, '/^(TRUE|FALSE)$/i', "Invalid traditionalDepartment filter value.");

            if (strtoupper($traditionalDepartment) === 'TRUE') {
                $deptWhere = "ia_ref_organization_department_division.CAMPUS_GROUP_CD='O'";
            }
        }
        $where[] = $deptWhere;

        $whereStr = "";
        if (sizeof($where) > 0) {
            $whereStr = " AND " . implode(" AND ", $where);
        }

        $results = $this->dbh->queryall_array($this->getEmployeeQuery($whereStr, $deptWhere));

        $payload = array();
        foreach ($results as $row) {
            // while($row = $sql->fetchrow_assoc()) {
            $payload = $this->buildRecord($row);
        }

        // $payload = $this->getEmployeeByPidm($pidm);

        if (sizeof($payload) <= 0) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        //check whether there are sub objects request and call resources to get additional payload
        $subObjects = $request->getSubObjects();
        if (isset($subObjects) && sizeof($subObjects) > 0) {
            // $uid = $payload['uniqueId'];
            // $payload = $this->addSubObjects($payload,$subObjects,$uid);
            $payload = $this->addSubObjects($payload, $subObjects, true);


        }

        $response->setPayload($payload);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }

//add the sub objects to the payload
    private function addSubObjects($payload, $subObjects, $singleResource, $pageSize = 1)
    {

        if ($singleResource) {

            $pidm = $payload['pidm'];

            //calling single resource for all sub objects
            foreach (array_keys($subObjects) as $subObj) {
                if ($subObj == "jobs" || $subObj === 'includeSupplementJobs') {
                    $jobs = $this->callResource(
                        'job.v1.muid', array(
                            'params' => array(
                                'muid' => "pidm=" . $pidm,
                                'includeSupplement' => $subObj === 'includeSupplementJobs' ? 'true' : 'false'
                            )
                        )
                    );
                    $payload['jobs'] = $jobs->getPayload();
                } elseif ($subObj == "person") {
                    $person = $this->callResource(
                        'person.read.alternate', array('params' => array('personID' => "pidm=" . $pidm))
                    );
                    $payload['person'] = $person->getPayload();
                }
            }
        } else {
            $pidms = array();
            foreach ($payload as $employee) {
                $pidms[] = $employee['pidm'];
            }
            $pidmStr = implode(",", $pidms);


            $returnJob = $returnPerson = false;

            foreach (array_keys($subObjects) as $subObj) {
                switch ($subObj) {
                    case 'jobs' || 'includeSupplementJobs':
                        $returnJob = true;
                        $offset = 1;
                        $limit = $pageSize;
                        $jobPidms = array();
                        $jobPayload = array();
                        do {

                            $jobsResponse = $this->callResource(
                                'job.v1',
                                array('options' => array(
                                    'pidm' => $pidmStr,
                                    'includeSupplement' => $subObj === 'includeSupplementJobs' ? 'true' : 'false',
                                    'limit' => $limit,
                                    'offset' => $offset
                                ))
                            );

                            // $limit = $jobsResponse->getResponseLimit();
                            // $offset = $jobsResponse->getResponseOffset();
                            $total = $jobsResponse->getTotalObjects();

                            $jobPidms = array_merge($jobPidms, array_column($jobsResponse->getPayload(), 'pidm'));
                            $jobPayload = array_merge($jobPayload, $jobsResponse->getPayload());

                            $offset = $offset + $limit;

                        } while ($offset <= $total);

                        break;

                    case 'person':
                        $returnPerson = true;
                        $person = $this->callResource(
                            'person.read.collection.v2', array('options' => array('pidm' => $pidmStr))
                        );
                        $personPayload = $person->getPayload();
                        $personPidms = array_column($personPayload, 'pidm');

                        break;
                }
            }

            for ($i = 0; $i < sizeof($payload); $i++) {
                if ($returnJob) {
                    $payload[$i]['jobs'] = array();
                    $keys = array_keys($jobPidms, $payload[$i]['pidm']);
                    foreach ($keys as $key) {
                        $payload[$i]['jobs'][] = $jobPayload[$key];
                    }

                }
                if ($returnPerson) {
                    $payload[$i]['person'] = null;
                    $keys = array_keys($personPidms, $payload[$i]['pidm']);
                    if ($keys[0] !== null) {
                        $payload[$i]['person'] = $personPayload[$keys[0]];
                    }

                }
            }


        }
        return $payload;
    }


    private function getEmployeeQuery($whereStr, $deptWhere)
    {

        $query = "select
                    pebempl_pidm,
                    pebempl_ecls_code,
                    to_char(pebempl_first_hire_date, 'YYYY-MM-DD') as first_hire_date,
                    pebempl_coas_code_home,
                    pebempl_coas_code_dist,
                    pebempl_orgn_code_dist,
                    pebempl_lcat_code,
                    pebempl_bcat_code,
                    to_char(pebempl_adj_service_date, 'YYYY-MM-DD') as service_date,
                    to_char( pebempl_seniority_date, 'YYYY-MM-DD') as seniority_date,
                    pebempl_trea_code,
                    to_char(pebempl_activity_date, 'YYYY-MM-DD') as activity_date,
                    pebempl_wkpr_code,
                    pebempl_flsa_ind,
                    pebempl_internal_ft_pt_ind,
                    pebempl_dicd_code,
                    to_char(pebempl_first_work_date, 'YYYY-MM-DD') as first_work_date,
                    to_char(pebempl_last_work_date, 'YYYY-MM-DD') as last_work_date,
                    pebempl_jbln_code,
                    pebempl_camp_code,
                    pebempl_coll_code,
                    to_char(pebempl_current_hire_date,'YYYY-MM-DD') as start_date,
                    szbuniq_unique_id,
                    szbuniq_banner_id,
                    ref_ft.eclass_desc,
                    ref_ft.ft_pt_group_code,
                    ref_ft.ft_pt_group_desc,
                    pebempl_egrp_code,
                    (select ptvegrp_desc from ptvegrp a
                    where a.ptvegrp_code = pebempl_egrp_code
                    and pebempl_egrp_code is not null
                    and rownum = 1) AS ptvegrp_desc,
                    FZ_GET_FACULTY_RANK_CODE(pebempl_pidm) as faculty_rank,
                    fz_get_faculty_rank_desc(pebempl_pidm) as faculty_rank_desc,
                    FZ_GET_LIBRARIAN_RANK_CODE(pebempl_pidm) AS librarian_rank,
                    FZ_GET_LIBRARIAN_RANK_DESC(pebempl_pidm) as librarian_rank_desc,
                    (select perappt_tenure_code from perappt a
                      where a.perappt_pidm =  pebempl_pidm
                      and a.perappt_action_date =  (select max(b.perappt_action_date) from perappt b
                                                    where a.perappt_pidm = b.perappt_pidm)
                    ) as tenure_code,
                    (select ptrtenr_desc from ptrtenr,perappt a
                      where ptrtenr_code = a.perappt_tenure_code
                      and a.PERAPPT_pidm = pebempl_pidm
                      and a.perappt_action_date = (select max(b.perappt_action_date) from perappt b
                                                   where a.perappt_pidm = b.perappt_pidm)
                      and rownum=1
                    ) as tenure_description,
                    (select pprccmt_text from pprccmt
                      where pprccmt_pidm = pebempl_pidm
                      and pprccmt_cmty_code = 'CIP'
                      and rownum=1
                    ) as cip_code,
                    (select ptvcmty_desc from ptvcmty,pprccmt
                      where pprccmt_pidm = pebempl_pidm
                      and pprccmt_cmty_code = 'CIP'
                      and pprccmt_cmty_code = ptvcmty_code
                      and rownum=1
                    ) as cip_description,
                    (select pprcert_cert_code from pprcert
                      where pprcert_pidm = pebempl_pidm
                      and rownum=1
                    ) AS grad_level_standing_code,
                    (select ptrcert_desc from pprcert, ptrcert
                      where pprcert_cert_code=ptrcert_code
                      and pprcert_pidm = pebempl_pidm
                      and rownum=1
                    ) as grad_level_standing_desc,
                    to_char(pprcert.PPRCERT_CERT_DATE,'YYYY-MM-DD') as grad_level_cert_date,
                    to_char(pprcert.pprcert_expire_date,'YYYY-MM-DD') AS grad_level_exp_date,
                    PEBEMPL_ORGN_CODE_HOME as home_org_code,
                    nvl(f_orgn_hier2_fnc(PEBEMPL_COAS_CODE_HOME,PEBEMPL_ORGN_CODE_HOME,5),
                    f_orgn_hier2_fnc(PEBEMPL_COAS_CODE_HOME,PEBEMPL_ORGN_CODE_HOME,4)) as home_org_description,
                    standardized_division_cd      AS standardized_division_code,
                    standardized_division_nm      AS standardized_division_name,
                    standardized_department_cd   AS standardized_department_code,
                    standardized_department_nm   AS standardized_department_name,
                    dualCodes.perfacc_facc_code" . $this->getQueryBody($deptWhere) . " " . $whereStr;
                    return $query;
    }

    //query main body. put it in a function in case this needs to be dynamic in the future.
    private function getQueryBody($deptWhere)
    {
        return " from
        szbuniq,
        ia_bi_lkp_employee_ftpt_tp ref_ft,
        pebempl left join (select perfacc_pidm, perfacc_facc_code from
                            perfacc where
                            perfacc_facc_code like 'D%')
                          dualCodes on pebempl_pidm = dualCodes.perfacc_pidm
                left join (select pprccmt_pidm, pprccmt_cmty_code from
                            pprccmt left join ptvcmty on pprccmt_cmty_code = ptvcmty_code
                            where
                            pprccmt_cmty_code='ACL'
                            or pprccmt_cmty_code='ASL'
                            or pprccmt_cmty_code='PRL'
                            or pprccmt_cmty_code='INL')
                          cip on pebempl_pidm = cip.pprccmt_pidm
                left join (select PPRCERT_CERT_DATE , PPRCERT_EXPIRE_DATE, pprcert_pidm from pprcert) pprcert on pebempl_pidm = pprcert.pprcert_pidm
                full join (SELECT
                unique(ia_ref_organization_level_5.organization_level_5_cd),
                ia_ref_organization_level_5.division_cd,
                ia_ref_organization_level_5.department_cd,
                ia_ref_standardize_division.standardized_division_cd,
                ia_ref_standardize_division.standardized_division_nm,
                ia_ref_standardize_department.standardized_department_cd,
                ia_ref_standardize_department.standardized_department_nm,
                ia_ref_standardize_department.campus_group_cd
            FROM
                ia_ref_organization_level_5, ia_ref_standardize_division, ia_ref_standardize_department
            WHERE
                ia_ref_organization_level_5.effective_from_dt <= sysdate
                AND ia_ref_organization_level_5.effective_to_dt >= sysdate
                AND ia_ref_standardize_division.effective_to_dt >= sysdate
                AND ia_ref_standardize_division.effective_from_dt <= sysdate
                AND ia_ref_standardize_department.effective_to_dt >= sysdate
                AND ia_ref_standardize_department.effective_from_dt <= sysdate
                AND ia_ref_standardize_division.division_cd = ia_ref_organization_level_5.division_cd
                AND ia_ref_standardize_department.department_cd = ia_ref_organization_level_5.department_cd
        ) ia_ref_organization_department_division
                  ON ia_ref_organization_department_division.organization_level_5_cd = pebempl.pebempl_orgn_code_home
                  AND  $deptWhere
        where
        szbuniq_pidm = pebempl_pidm
        and pebempl_ecls_code=ref_ft.eclass_code
        and pebempl_empl_status <> 'T'
         ";

    }


    //build json record from the raw database record
    protected function buildRecord($row)
    {
        if ($row['faculty_rank']) {
            $rank = $row['faculty_rank'];
            $rank_desc = $row['faculty_rank_desc'] ? $row['faculty_rank_desc'] : "";
        } elseif ($row['librarian_rank']) {
            $rank = $row['librarian_rank'];
            $rank_desc = $row['librarian_rank_desc'] ? $row['librarian_rank_desc'] : "";
        } else {
            $rank = null;
            $rank_desc = null;
        }
        $record = array(
            'pidm' => $row['pebempl_pidm'],
            'uniqueId' => $row['szbuniq_unique_id'],
            'plusNumber' => $row['szbuniq_banner_id'],
            'startDate' => $row['start_date'],
            'classificationCode' => $row['pebempl_ecls_code'],
            'classificationDescription' => $row['eclass_desc'],
            'groupCode' => $row['pebempl_egrp_code'],
            'groupDescription' => $row['ptvegrp_desc'],
            'fullTimeCode' => $row['ft_pt_group_code'],
            'fullTimeDescription' => $row['ft_pt_group_desc'],
            'professionalLibrarian' => $row['librarian_rank'] ? 'true' : 'false',
            'rankCode' => $rank,
            'rankDescription' => $rank_desc,
            'tenureCode' => $row['tenure_code'],
            'tenureDescription' => $row['tenure_description'],
            'cipCode' => $row['cip_code'],
            'cipDescription' => $row['cip_description'],
            'gradLevelCode' => $row['grad_level_standing_code'],
            'gradLevelDescription' => $row['grad_level_standing_desc'],
            'gradLevelCertDate' => $row['grad_level_cert_date'],
            'gradLevelExpDate' => $row['grad_level_exp_date'],
            'homeOrgCode' => $row['home_org_code'],
            'homeOrgDescription' => $row['home_org_description'],
            'standardizedDivisionCode' => $row['standardized_division_code'],
            'standardizedDivisionName' => $row['standardized_division_name'],
            'standardizedDepartmentCode' => $row['standardized_department_code'],
            'standardizedDepartmentName' => $row['standardized_department_name'],
            'dualAppointmentCode' => $row['perfacc_facc_code'],
            'firstHireDate' => $row['first_hire_date'],
            'chartOfAccountsCode' => $row['pebempl_coas_code_home'],
            'chartOfAccountsDistribution' => $row['pebempl_coas_code_dist'],
            'orgCodeDistribution'=> $row['pebempl_orgn_code_dist'],
            'leaveCategoryCode' => $row['pebempl_lcat_code'],
            'benefitCategoryCode' => $row['pebempl_bcat_code'],
            'serviceDate' => $row['service_date'],
            'seniorityDate' => $row['seniority_date'],
            'terminatedReasonCode' => $row['pebempl_trea_code'],
            'activityDate' => $row['activity_date'],
            'workPeriodCode' => $row['pebempl_wkpr_code'],
            'flsaIndicator' => $row['pebempl_flsa_ind'],
            'internalFullTimePartTimeIndicator' => $row['pebempl_internal_ft_pt_ind'],
            'districtCode' => $row['pebempl_dicd_code'],
            'firstWorkDate' => $row['first_work_date'],
            'lastWorkDate' => $row['last_work_date'],
            'jobLocationCode' => $row['pebempl_jbln_code'],
            'campusCode' => $row['pebempl_camp_code'],
            'collegeCode' => $row['pebempl_coll_code'],
        );

        return $record;
    }

    //Build "in" clause for SQL queries
    protected function buildInString($array, $isString)
    {
        $result = "";
        foreach ($array as $element) {
            if ($isString) {
                $result = $result . ",'" . strtoupper($element) . "'";
            } else {
                $result = $result . "," . strtoupper($element);
            }
        }
        $result = substr($result, 1);
        return $result;

    }

    //Get regex patterns for input params
    protected function getPattern($val)
    {
        if ($val == 'pidm') {
            return '/^\d{1,8}$/';
        } else {
            if ($val == 'uniqueId') {
                return '/^\w{1,8}$/';
            }
        }
    }

    //validate input params based on regex
    protected function validateInput($input, $pattern, $errMesg)
    {

        if (is_array($input)) {
            foreach ($input as $value) {

                if (!preg_match($pattern, $value)) {
                    throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
                }
            }
        } else {
            if (!preg_match($pattern, $input)) {
                throw new \MiamiOH\RESTng\Exception\BadRequest($errMesg);
            }
        }
    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function updateEmployee()
    {

        $request = $this->getRequest();
        $response = $this->getResponse();
        $pidm = $request->getResourceParam('pidm');

        $user = $this->getApiUser();

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $validator = RESTngValidatorFactory::make($request->getData(), $this->updateRules, $this->messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $payLoad['errors'][] = $errors->getMessageBag();
            $response->setPayLoad($payLoad);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $data['userId'] = strtoupper($user->getUsername());
        $data['dataOrigin'] = 'WebService';
        $data['pidm'] = $pidm;

        try{
        $employeeObject = EmployeeData::fromArray($data);
        $employeeSql = new EmployeeSQL();
        $employeeSql->updateEmployee($employeeObject);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        $response->setPayload(['1 record updated']);
        $response->setStatus(\MiamiOH\RESTng\App::API_OK);

        return $response;

    }

    /**
     * @return \MiamiOH\RESTng\Util\Response
     * @throws \Exception
     */
    public function createEmployee()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $user = $this->getApiUser();

        $data = $request->getData();

        # Make sure data is not empty
        if (empty($data)) {
            $payLoad['errors'][] = 'No Data';
            $status = \MiamiOH\RESTng\App::API_BADREQUEST;

            $response->setPayLoad($payLoad);
            $response->setStatus($status);
            return $response;
        }

        $validator = RESTngValidatorFactory::make($request->getData(), $this->rules, $this->messages);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $payLoad['errors'][] = $errors->getMessageBag();
            $response->setPayLoad($payLoad);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);
            return $response;
        }

        $data['userId'] = $user->getUsername();
        $data['dataOrigin'] = 'WebService';

        try{
        $employeeObject = EmployeeData::fromArray($data);
        $employeeSql = new EmployeeSQL();
        $employeeSql->createEmployee($employeeObject);
        } catch (\Exception $e) {
            $response->setPayload([$e->getMessage()]);
            $response->setStatus(\MiamiOH\RESTng\App::API_FAILED);
            return $response;
        }

        $response->setStatus(\MiamiOH\RESTng\App::API_CREATED);
        $response->setPayload(['1 record created.']);

        return $response;
    }


}
