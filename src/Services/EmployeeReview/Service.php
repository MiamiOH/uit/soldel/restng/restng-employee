<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-21
 * Time: 10:46
 */

namespace MiamiOH\RestngEmployee\Services\EmployeeReview;

use MiamiOH\RestngEmployee\Repositories\EmployeeReviewRepository;
use MiamiOH\RestngEmployee\Repositories\EmployeeReviewSQL;
use MiamiOH\RESTngIlluminateIntegration\RESTngEloquentFactory;
use MiamiOH\RestngEmployee\Repositories\MUIDRepositoryResourceCall;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use MiamiOH\RESTng\Legacy\DataSource;

class Service extends \MiamiOH\RESTng\Service
{
    /**
     * @var Request
     */
    private $request = null;

    /**
     * @var Response
     */
    private $response = null;

    /**
     * @var User
     */
    protected $user = null;

    /**
     * @var EmployeeReviewRepository
     */
    protected $repository = null;

    public function getDependencies()
    {
        date_default_timezone_set('US/Eastern');

        $this->user = $this->getApiUser();
        $this->response = $this->getResponse();
        $this->request = $this->getRequest();

        $muidCall = function (array $params = [], array $options = []) {
            return $this->callResource('muid.get', $params, $options);
        };

        $muidRepository = new MUIDRepositoryResourceCall($muidCall);

        $this->repository = new EmployeeReviewSQL($muidRepository);
    }

    /**
     * @throws \Exception
     */
    public function getEmployeeReview()
    {
        $this->getDependencies();

        $getService = new Get();

        $response = $getService->getEmployeeReview(
            $this->request,
            $this->response,
            $this->repository
        );

        return $response;
    }
}
