<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-08-21
 * Time: 10:46
 */

namespace MiamiOH\RestngEmployee\Services\EmployeeReview;

use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;
use MiamiOH\RestngEmployee\Repositories\EmployeeReviewRepository;

class Get
{
    private $rules = [
        'muid' => ['required', 'regex:/^(\w{1,8}|\d{1,8}|\+[0-9]{1,8})$/'],
        'fromCompletedDate' => ['date'],
        'toCompletedDate' => ['date']
    ];

    /**
     * @param Request $request
     * @param Response $response
     * @param EmployeeReviewRepository $repository
     * @return Response
     * @throws \MiamiOH\RESTng\Exception\BadRequest
     */
    public function getEmployeeReview(
        Request $request,
        Response $response,
        EmployeeReviewRepository $repository
    ) {
        $status = \MiamiOH\RESTng\App::API_OK;

        $options = $request->getOptions();

        $muid = $options['muid'] ?? '';
        $fromDate = $options['fromCompletedDate'] ?? '';
        $toDate = $options['toCompletedDate'] ?? '';

        //Validate input parameters
        $muid = trim($muid);

        $validator = RESTngValidatorFactory::make(
            [
                'muid' => $muid,
            ],
            $this->rules
        );


        if ($validator->fails()) {
            $response->setPayload([
                $validator->errors()->getMessages()
            ]);
            $response->setStatus(\MiamiOH\RESTng\App::API_BADREQUEST);

            return $response;
        }

        $payload = $repository->readEmployeeReview($muid, $fromDate, $toDate);

        if (empty($payload)) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        $response->setTotalObjects(count($payload));
        $response->setPayload($payload);
        $response->setStatus($status);

        return $response;
    }
}