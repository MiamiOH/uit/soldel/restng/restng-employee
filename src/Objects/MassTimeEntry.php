<?php
/**
 * Created by PhpStorm.
 * User: rajends
 * Date: 2019-05-29
 * Time: 13:30
 */

namespace MiamiOH\RestngEmployee\Objects;

use Exception;
use MiamiOH\RESTngIlluminateIntegration\RESTngValidatorFactory;

class MassTimeEntry
{
    /**
     *
     */
    const ORACLE_DATE_FORMAT = 'Y-m-d H:i:s';

    /**
     * @var string
     */
    private $year = '';

    /**
     * @var string
     */
    private $payrollCode = '';

    /**
     * @var string
     */
    private $payrollNumber = '';

    /**
     * @var string
     */
    private $bannerId = '';

    /**
     * @var string
     */
    private $position = '';

    /**
     * @var string
     */
    private $suffix = '';

    /**
     * @var \DateTime
     */
    private $effectiveDate = null;

    /**
     * @var string
     */
    private $earnCode = '';

    /**
     * @var string
     */
    private $shift = '';

    /**
     * @var \DateTime
     */
    private $beginDate = null;

    /**
     * @var \DateTime
     */
    private $endDate = null;

    /**
     * @var string
     */
    private $specialRate = '';

    /**
     * @var string
     */
    private $hours = '';

    /**
     * @var string
     */
    private $chartOfAccountsCode = '';

    /**
     * @var string
     */
    private $accountIndexCode = '';

    /**
     * @var string
     */
    private $fundCode = '';

    /**
     * @var string
     */
    private $organizationCode = '';

    /**
     * @var string
     */
    private $accountCode = '';

    /**
     * @var string
     */
    private $programCode= '';

    /**
     * @var string
     */
    private $activityCode = '';

    /**
     * @var string
     */
    private $locationCode = '';

    /**
     * @var string
     */
    private $externalAccountCode = '';

    /**
     * @var string
     */
    private $percent = '';

    /**
     * @var \DateTime
     */
    private $activityDate = null;

    /**
     * @var string
     */
    private $projectCode = '';


    /**
     * @var string
     */
    private $costTypeCode = '';
    
    /**
     * @var string
     */
    private $changeIndicator= '';

    /**
     * @var string
     */
    private $deemedHours = '';

    /**
     * @var string
     */
    private $surrogateId = '';

    /**
     * @var string
     */
    private $userId = '';


    /**
     * @var string
     */
    private $dataOrigin = '';

    /**
     * @param $year
     * @return MassTimeEntry
     */
    public function setYear(string $year): MassTimeEntry
    {
        $this->year = $year;
        return $this;
    }

    /**
     * @return string
     */
    public function getYear(): string
    {
        return $this->year;
    }

    /**
     * @param $payrollCode
     * @return MassTimeEntry
     */
    public function setPayrollCode(string $payrollCode): MassTimeEntry
    {
        $this->payrollCode = $payrollCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayrollCode(): string
    {
        return $this->payrollCode;
    }

    /**
     * @param $payrollNumber
     * @return MassTimeEntry
     */
    public function setPayrollNumber(string $payrollNumber): MassTimeEntry
    {
        $this->payrollNumber = $payrollNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getPayrollNumber(): string
    {
        return $this->payrollNumber;
    }

    /**
     * @param $bannerId
     * @return MassTimeEntry
     */
    public function setBannerId(string $bannerId): MassTimeEntry
    {
        $this->bannerId = $bannerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getBannerId(): string
    {
        return $this->bannerId;
    }

    /**
     * @param string $position
     * @return MassTimeEntry
     */
    public function setPosition(string $position): MassTimeEntry
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $suffix
     * @return MassTimeEntry
     */
    public function setSuffix(string $suffix): MassTimeEntry
    {
        $this->suffix = $suffix;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuffix(): string
    {
        return $this->suffix;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEffectiveDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->effectiveDate)) {
            return '';
        }

        return $this->effectiveDate->format($format);
    }

    /*
     * @param string $effectiveDate
     * @return MassTimeEntry
     */
    public function setEffectiveDate(string $effectiveDate): MassTimeEntry
    {
        if (empty($effectiveDate)) {
            $this->effectiveDate = null;
        } else {
            $this->effectiveDate = new \DateTime($effectiveDate);
        }

        return $this;
    }

    /**
     * @param string $earnCode
     * @return MassTimeEntry
     */
    public function setEarnCode(string $earnCode): MassTimeEntry
    {
        $this->earnCode = $earnCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getEarnCode(): string
    {
        return $this->earnCode;
    }

    /**
     * @param string $shift
     * @return MassTimeEntry
     */
    public function setShift(string $shift): MassTimeEntry
    {
        $this->shift = $shift;
        return $this;
    }

    /**
     * @return string
     */
    public function getShift(): string
    {
        return $this->shift;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getBeginDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->beginDate)) {
            return '';
        }

        return $this->beginDate->format($format);
    }

    /*
     * @param string $beginDate
     * @return MassTimeEntry
     */
    public function setBeginDate(string $beginDate): MassTimeEntry
    {
        if (empty($beginDate)) {
            $this->beginDate = null;
        } else {
            $this->beginDate = new \DateTime($beginDate);
        }

        return $this;
    }

    /**
     * @param string $format
     * @return string
     */
    public function getEndDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        if (empty($this->endDate)) {
            return '';
        }

        return $this->endDate->format($format);
    }

    /*
     * @param string $endDate
     * @return MassTimeEntry
     */
    public function setEndDate(string $endDate): MassTimeEntry
    {
        if (empty($endDate)) {
            $this->endDate = null;
        } else {
            $this->endDate = new \DateTime($endDate);
        }

        return $this;
    }

    /**
     * @param string $specialRate
     * @return MassTimeEntry
     */
    public function setSpecialRate(string $specialRate): MassTimeEntry
    {
        $this->specialRate = $specialRate;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialRate(): string
    {
        return $this->specialRate;
    }

    /**
     * @param string $hours
     * @return MassTimeEntry
     */
    public function setHours(string $hours): MassTimeEntry
    {
        $this->hours = $hours;
        return $this;
    }

    /**
     * @return string
     */
    public function getHours(): string
    {
        return $this->hours;
    }

    /**
     * @return string
     */
    public function getChartOfAccountsCode(): string
    {
        return $this->chartOfAccountsCode;
    }

    /**
     * @param string $chartOfAccountsCode
     * @return MassTimeEntry
     */
    public function setChartOfAccountsCode(string $chartOfAccountsCode): MassTimeEntry
    {
        $this->chartOfAccountsCode = $chartOfAccountsCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountIndexCode(): string
    {
        return $this->accountIndexCode;
    }

    /**
     * @param string $accountIndexCode
     * @return MassTimeEntry
     */
    public function setAccountIndexCode(string $accountIndexCode): MassTimeEntry
    {
        $this->accountIndexCode = $accountIndexCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getFundCode(): string
    {
        return $this->fundCode;
    }

    /**
     * @param string $fundCode
     * @return MassTimeEntry
     */
    public function setFundCode(string $fundCode): MassTimeEntry
    {
        $this->fundCode = $fundCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationCode(): string
    {
        return $this->organizationCode;
    }

    /**
     * @param string $organizationCode
     * @return MassTimeEntry
     */
    public function setOrganizationCode(string $organizationCode): MassTimeEntry
    {
        $this->organizationCode = $organizationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountCode(): string
    {
        return $this->accountCode;
    }

    /**
     * @param string $accountCode
     * @return MassTimeEntry
     */
    public function setAccountCode(string $accountCode): MassTimeEntry
    {
        $this->accountCode = $accountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProgramCode(): string
    {
        return $this->programCode;
    }

    /**
     * @param string $programCode
     * @return MassTimeEntry
     */
    public function setProgramCode(string $programCode): MassTimeEntry
    {
        $this->programCode = $programCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getActivityCode(): string
    {
        return $this->activityCode;
    }

    /**
     * @param string $activityCode
     * @return MassTimeEntry
     */
    public function setActivityCode(string $activityCode): MassTimeEntry
    {
        $this->activityCode = $activityCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocationCode(): string
    {
        return $this->locationCode;
    }

    /**
     * @param string $locationCode
     * @return MassTimeEntry
     */
    public function setLocationCode(string $locationCode): MassTimeEntry
    {
        $this->locationCode = $locationCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getProjectCode(): string
    {
        return $this->projectCode;
    }

    /**
     * @param string $projectCode
     * @return MassTimeEntry
     */
    public function setProjectCode(string $projectCode): MassTimeEntry
    {
        $this->projectCode = $projectCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCostTypeCode(): string
    {
        return $this->costTypeCode;
    }

    /**
     * @param string $costTypeCode
     * @return MassTimeEntry
     */
    public function setCostTypeCode(string $costTypeCode): MassTimeEntry
    {
        $this->costTypeCode = $costTypeCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getExternalAccountCode(): string
    {
        return $this->externalAccountCode;
    }

    /**
     * @param string $externalAccountCode
     * @return MassTimeEntry
     */
    public function setExternalAccountCode(string $externalAccountCode): MassTimeEntry
    {
        $this->externalAccountCode = $externalAccountCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getPercent(): string
    {
        return $this->percent;
    }

    /**
     * @param string $percent
     * @return MassTimeEntry
     */
    public function setPercent(string $percent): MassTimeEntry
    {
        $this->percent = $percent;
        return $this;
    }

    /**
     * @return string
     */
    public function getChangeIndicator(): string
    {
        return $this->changeIndicator;
    }

    /**
     * @param string $changeIndicator
     * @return MassTimeEntry
     */
    public function setChangeIndicator(string $changeIndicator): MassTimeEntry
    {
        $this->changeIndicator = $changeIndicator;
        return $this;
    }

    /**
     * @param string $deemedHours
     * @return MassTimeEntry
     */
    public function setDeemedHours(string $deemedHours): MassTimeEntry
    {
        $this->deemedHours = $deemedHours;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeemedHours(): string
    {
        return $this->deemedHours;
    }

    /**
     * @return string
     */
    public function getSurrogateId(): string
    {
        return $this->surrogateId;
    }

    /**
     * @param string $surrogateId
     * @return MassTimeEntry
     */
    public function setSurrogateId(string $surrogateId): MassTimeEntry
    {
        $this->surrogateId = $surrogateId;
        return $this;
    }

    /**
     * @return string
     */
    public function getDataOrigin(): string
    {
        return $this->dataOrigin;
    }

    /**
     * @param string $dataOrigin
     * @return MassTimeEntry
     */
    public function setDataOrigin(string $dataOrigin): MassTimeEntry
    {
        $this->dataOrigin = $dataOrigin;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return MassTimeEntry
     */
    public function setUserId(string $userId): MassTimeEntry
    {
        $this->userId = $userId;
        return $this;
    }

    /*
     * @param string $format
     * @return string
     */
    public function getActivityDate(string $format = self::ORACLE_DATE_FORMAT): string
    {
        return (new \DateTime('now'))->format($format);
    }

    private static $rules = [
        // required fields
        'year' => ['required', 'max:4'],
        'payrollCode' => ['required', 'max:2'],
        'payrollNumber' => ['required', 'numeric', 'regex:/^\d{1,3}(\.\d*)?$/'],
        'bannerId' => ['required', 'max:9'],
        'beginDate' => ['required', 'date'],
        'changeIndicator' => ['required', 'max:1'],

        // non required fields
        'effectiveDate' => ['date'],
        'position' => ['max:6'],
        'suffix' => ['max:2'],
        'earnCode' => ['max:3'],
        'shift' => ['max:1'],
        'accountCode' => ['max:6'],
        'accountIndexCode' => ['max:6'],
        'activityCode' => ['max:6'],
        'chartOfAccountsCode' => ['max:1'],
        'costTypeCode' => ['max:2'],
        'fundCode' => ['max:6'],
        'locationCode' => ['max:6'],
        'organizationCode' => ['max:6'],
        'programCode' => ['max:6'],
        'projectCode' => ['max:8'],
        'externalAccountCode' => ['max:60'],
        'endDate' => ['date'],
        'specialRate' => ['numeric', 'regex:/^\d{1,12}(\.\d*)?$/'],
        'hours' => ['numeric', 'regex:/^\d{1,6}(\.\d*)?$/'],
        'percent' => ['numeric', 'regex:/^\d{1,5}(\.\d*)?$/'],
        'deemedHours' => ['numeric', 'regex:/^\d{1,6}(\.\d*)?$/'],

        // non user-defined field
        'dataOrigin' => ['max:30'],
        'userId' => ['max:30']
    ];

    /**
     * @param array $data
     * @return MassTimeEntry
     * @throws \Exception
     */
    public static function fromArray(array $data): self
    {
        self::validateArray($data);

        $massTimeEntry = new self();

        try {
            foreach ($data as $key => $val) {
                $massTimeEntry->{'set' . ucfirst($key)}(strtoupper($val));
            }
        } catch (\Exception $e) {
            throw new \InvalidArgumentException("Invalid key for Job Labor Distribution: $key. " . $e->getMessage());
        }

        return $massTimeEntry;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    public static function validateArray(array $data): void
    {
        $validator = RESTngValidatorFactory::make($data, self::$rules);

        if ($validator->fails()) {
            throw new \InvalidArgumentException(
                'Validation failed. '
                . implode(' ', $validator->errors()->all())
            );
        }
    }


}