<?php

/*
-----------------------------------------------------------
FILE NAME: getOneEmployeeTest.php

Copyright (c) 2016 Miami University, All Rights Reserved.

Miami University grants you ("Licensee") a non-exclusive, royalty free,
license to use, modify and redistribute this software in source and
binary code form, provided that i) this copyright notice and license
appear on all copies of the software; and ii) Licensee does not utilize
the software in a manner which is disparaging to Miami University.

This software is provided "AS IS" and any express or implied warranties,
including, but not limited to, the implied warranties of merchantability
and fitness for a particular purpose are disclaimed. It has been tested
and is believed to work as intended within Miami University's
environment. Miami University does not warrant this software to work as
designed in any other environment.

AUTHOR: Kelly Geng

DESCRIPTION:  Unit Tests for Testing the GET Functionality of
              the Employee (Single) Web Service

ENVIRONMENT DEPENDENCIES: PHP Unit

AUDIT TRAIL:

DATE        UniqueID
08/02/2016  gengx       Initial File
08/23/2016  schmidee    Update for Home Org Return
08/24/2016  schmdiee    Update for Standard Division Code and Name Return.
09/06/2016  schmidee    Update for Standard Department Code and Name Return.
10/20/2016  schmidee    Corrected Unit Tests for adding in Dual Appointment Code.


*/

// include_once 'sthMockStatements.php';

namespace MiamiOH\RestngEmployee\Tests\Unit;

use MiamiOH\RESTng\App;

class GetOneEmployeeTest extends \MiamiOH\RESTng\Testing\TestCase
{


    private $dbh, $request, $response, $bannerUtil, $employee, $queryallRecords;

    protected function setUp()
    {

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam', 'getOptions', 'getSubObjects', 'getResourceParamKey'))
            ->getMock();

        // $request->method('getResourceParam')
        //         ->with($this->anything())
        //         ->will($this->returnCallback(array($this, 'returnResourceParam')));

        // $request->method('getOptions')
        //         ->will($this->returnCallback(array($this, 'returnOptions')));

        //set up the mock dbh
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);


        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();


        $this->employee = new \MiamiOH\RestngEmployee\Services\Employee();

        $this->employee->setDatabase($db);
        $this->employee->setBannerUtil($this->bannerUtil);
        $this->employee->setRequest($this->request);

    }

    public function testGetOneEmployeeByUid()
    {

        $this->request->method('getResourceParam')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            // ->will($this->onConsecutiveCalls('gengx','covertka'));
            ->willReturn('covertka');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            ->willReturn('uniqueId');


        $this->request->method('getSubObjects')
            ->willReturn(array());

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $person->method('getPidm')
            // ->will($this->onConsecutiveCalls(1225918,237663));
            ->willReturn(237663);

        $this->bannerUtil->method('getId')
            ->with('uniqueId', $this->anything())
            ->willReturn($person);

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryOneEmployeeStaff')));


        $this->response = $this->employee->getOneEmployee();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());

        $this->assertEquals($payload, $this->mockOneEmployeeResultStaff());
    }


    public function testGetOneEmployeeByUidInvalidUid()
    {


        $this->request->method('getResourceParam')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            // ->will($this->onConsecutiveCalls('gengx','covertka'));
            ->willReturn('cover$@*');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            ->willReturn('uniqueId');

        // $this->setExpectedException('MiamiOH\RESTng\Exception\BadRequest');
        try {
            $this->response = $this->employee->getOneEmployee();
            $this->fail("Expected exception has not been raised.");

        } catch (\Exception $ex) {
            $this->assertEquals($ex->getMessage(), "Invalid unique ID.");
        }

    }

    public function testGetOneEmployeeByPidm()
    {

        $this->request->method('getResourceParam')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            // ->will($this->onConsecutiveCalls('gengx','covertka'));
            ->willReturn(101273);

        $this->request->method('getResourceParamKey')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            ->willReturn('pidm');


        $this->request->method('getSubObjects')
            ->willReturn(array());

        $person = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerId')
            ->setMethods(array('getPidm'))
            ->getMock();

        $person->method('getPidm')
            // ->will($this->onConsecutiveCalls(1225918,237663));
            ->willReturn(101273);

        $this->bannerUtil->method('getId')
            ->with('pidm', $this->anything())
            ->willReturn($person);

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->method('queryall_array')
            ->will($this->returnCallback(array($this, 'mockQueryOneEmployeeFaculty')));


        $this->response = $this->employee->getOneEmployee();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());

        $this->assertEquals($payload, $this->mockOneEmployeeResultFaculty());
    }

    public function testGetOneEmployeeByPidmInvalidPidm()
    {


        $this->request->method('getResourceParam')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            // ->will($this->onConsecutiveCalls('gengx','covertka'));
            ->willReturn('&^234$@*');

        $this->request->method('getResourceParamKey')
            ->with('muid')
            // ->will($this->returnCallback(array($this, 'mockResourceParamValidPim')));
            ->willReturn('pidm');


        // $this->setExpectedException('MiamiOH\RESTng\Exception\BadRequest');
        try {
            $this->response = $this->employee->getOneEmployee();
            $this->fail("Expected exception has not been raised.");

        } catch (\Exception $ex) {
            $this->assertEquals($ex->getMessage(), "Invalid pidm.");
        }

    }

    public function mockOneEmployeeResultStaff()
    {
        return array(
            'pidm' => 237663,
            'uniqueId' => 'COVERTKA',
            'plusNumber' => '+00237663',
            'startDate' => '1991-09-19',
            'classificationCode' => 'AF',
            'classificationDescription' => 'Administrative Full-time',
            'groupCode' => null,
            'groupDescription' => null,
            'fullTimeCode' => 'FT',
            'fullTimeDescription' => 'Full-Time Employee',
            'professionalLibrarian' => 'false',
            'rankCode' => null,
            'rankDescription' => null,
            'tenureCode' => null,
            'tenureDescription' => null,
            'cipCode' => null,
            'cipDescription' => null,
            'gradLevelCode' => null,
            'gradLevelDescription' => null,
            'gradLevelCertDate' => null,
            'gradLevelExpDate' => null,
            'homeOrgCode' => 'BIO99',
            'homeOrgDescription' => 'Biology',
            'standardizedDivisionCode' => 'FBS',
            'standardizedDivisionName' => 'Farmer School of Business',
            'standardizedDepartmentCode' => 'CSE',
            'standardizedDepartmentName' => 'Computer Science & Software Engineering',
            'dualAppointmentCode' => 'DENG',
            'firstHireDate' => null,
            'chartOfAccountsCode' => null,
            'chartOfAccountsDistribution' => null,
            'orgCodeDistribution' => null,
            'leaveCategoryCode' => null,
            'benefitCategoryCode' => null,
            'serviceDate' => null,
            'seniorityDate' => null,
            'terminatedReasonCode' => null,
            'activityDate' => null,
            'workPeriodCode' => null,
            'flsaIndicator' => null,
            'internalFullTimePartTimeIndicator' => null,
            'districtCode' => null,
            'firstWorkDate' => null,
            'lastWorkDate' => null,
            'jobLocationCode' => null,
            'campusCode' => null,
            'collegeCode' => null
        );

    }


    public function mockQueryOneEmployeeStaff()
    {
        return array(
            array(
                'pebempl_pidm' => 237663,
                'pebempl_ecls_code' => 'AF',
                'start_date' => '1991-09-19',
                'szbuniq_unique_id' => 'COVERTKA',
                'szbuniq_banner_id' => '+00237663',
                'eclass_desc' => 'Administrative Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => null,
                'faculty_rank_desc' => null,
                'librarian_rank' => null,
                'librarian_rank_desc' => null,
                'tenure_code' => null,
                'tenure_description' => null,
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => null,
                'grad_level_standing_desc' => null,
                'grad_level_cert_date' => null,
                'grad_level_exp_date' => null,
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Biology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,

            )
        );

    }

    public function mockOneEmployeeResultFaculty()
    {
        return array(
            'pidm' => 101273,
            'uniqueId' => 'PLATTGJ',
            'plusNumber' => '+00101273',
            'startDate' => '1993-08-17',
            'classificationCode' => 'FF',
            'classificationDescription' => 'Faculty Full-time',
            'groupCode' => null,
            'groupDescription' => null,
            'fullTimeCode' => 'FT',
            'fullTimeDescription' => 'Full-Time Employee',
            'professionalLibrarian' => 'false',
            'rankCode' => 'PR',
            'rankDescription' => 'Professor',
            'tenureCode' => 'T',
            'tenureDescription' => 'Tenured',
            'cipCode' => null,
            'cipDescription' => null,
            'gradLevelCode' => 'GSAP',
            'gradLevelDescription' => 'Grad School Level A Permanent',
            'gradLevelCertDate' => '2010-06-22',
            'gradLevelExpDate' => null,
            'homeOrgCode' => 'BIO99',
            'homeOrgDescription' => 'Biology',
            'standardizedDivisionCode' => 'FBS',
            'standardizedDivisionName' => 'Farmer School of Business',
            'standardizedDepartmentCode' => 'CSE',
            'standardizedDepartmentName' => 'Computer Science & Software Engineering',
            'dualAppointmentCode' => 'DENG',
            'dualAppointmentCode' => 'DENG',
            'firstHireDate' => null,
            'chartOfAccountsCode' => null,
            'chartOfAccountsDistribution' => null,
            'orgCodeDistribution' => null,
            'leaveCategoryCode' => null,
            'benefitCategoryCode' => null,
            'serviceDate' => null,
            'seniorityDate' => null,
            'terminatedReasonCode' => null,
            'activityDate' => null,
            'workPeriodCode' => null,
            'flsaIndicator' => null,
            'internalFullTimePartTimeIndicator' => null,
            'districtCode' => null,
            'firstWorkDate' => null,
            'lastWorkDate' => null,
            'jobLocationCode' => null,
            'campusCode' => null,
            'collegeCode' => null
        );

    }


    public function mockQueryOneEmployeeFaculty()
    {
        return array(
            array(
                'pebempl_pidm' => 101273,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '1993-08-17',
                'szbuniq_unique_id' => 'PLATTGJ',
                'szbuniq_banner_id' => '+00101273',
                'eclass_desc' => 'Faculty Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => 'PR',
                'faculty_rank_desc' => 'Professor',
                'librarian_rank' => null,
                'librarian_rank_desc' => null,
                'tenure_code' => 'T',
                'tenure_description' => 'Tenured',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => 'GSAP',
                'grad_level_standing_desc' => 'Grad School Level A Permanent',
                'grad_level_cert_date' => '2010-06-22',
                'grad_level_exp_date' => null,
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Biology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
            )
        );

    }


}
