<?php
namespace MiamiOH\RestngEmployee\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\TestCase;

class GetEmployeesV2Test extends TestCase
{


    /*************************/
    /**********Set Up*********/
    /*************************/
    private $dbh, $request, $response, $bannerUtil, $employee, $queryallRecords;

    // set up method which is automatically called by PHPUnit before every test method:
    protected function setUp()
    {

        $api = $this->getMockBuilder('\MiamiOH\RESTng\Util\API')
            ->setMethods(array('newResponse', 'callResource'))
            ->getMock();

        $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

        //set up the mock request:
        $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array(
                'getResourceParam',
                'getOptions',
                'getSubObjects',
                'getResourceParamKey',
                'getOffset',
                'getLimit'
            ))
            ->getMock();


        //set up the mock dbh
        $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();


        $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

        $db->method('getHandle')->willReturn($this->dbh);


        $this->bannerUtil = $this->getMockBuilder('\MiamiOH\RESTng\Service\Extension\BannerUtil')
            ->setMethods(array('getId'))
            ->getMock();


        $this->employee = new \MiamiOH\RestngEmployee\Services\EmployeeV2();

        $this->employee->setDatabase($db);
        $this->employee->setBannerUtil($this->bannerUtil);
        $this->employee->setRequest($this->request);

    }

    /**********************************/
    /**********Unique ID Tests*********/
    /**********************************/

    /********** Unique ID Single Tests *********/

    /**
     *  Test to get Employees by Unique ID (Single)
     */

    public function testEmployeeByUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Unique ID Single Employee Test
     */
    public function mockOptionsUniqueID()
    {
        return array('uniqueId' => ['test1']);
    }

    /**
     * Mock Database Return for Total Single Employee Test
     */
    public function mockQueryTotalSingle()
    {
        return array(
            array(
                'total' => 1,
            ),
        );
    }

    /**
     * Mock Database Return for Single Employee Test
     */
    public function mockQuerySingle()
    {
        return array(
            array(
                'pebempl_pidm' => 00000000,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2016-01-01',
                'szbuniq_unique_id' => 'TEST1',
                'szbuniq_banner_id' => '+00000000',
                'eclass_desc' => 'Faculty Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => 'AS',
                'faculty_rank_desc' => "Assistant Professor",
                'librarian_rank' => null,
                'librarian_rank_desc' => null,
                'tenure_code' => 'TT',
                'tenure_description' => 'Tenure Track',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => 'GSA',
                'grad_level_standing_desc' => "Grad School Level A",
                'grad_level_cert_date' => "2016-01-01",
                'grad_level_exp_date' => "2999-11-01",
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Microbiology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Expected Return from Unique ID Single Employee Test
     */
    public function returnEmployeeSingle()
    {
        return array(
            array(
                "pidm" => 00000000,
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "groupCode" => null,
                "groupDescription" => null,
                "fullTimeCode" => "FT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "false",
                "rankCode" => "AS",
                "rankDescription" => "Assistant Professor",
                "tenureCode" => "TT",
                "tenureDescription" => "Tenure Track",
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => "GSA",
                "gradLevelDescription" => "Grad School Level A",
                "gradLevelCertDate" => "2016-01-01",
                "gradLevelExpDate" => "2999-11-01",
                'homeOrgCode' => 'BIO99',
                'homeOrgDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'dualAppointmentCode' => 'DENG',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null
            )
        );
    }

    /**
     *  Invalid Unique ID (Single)
     */

    public function testInvalidUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Unique ID Single Test
     */
    public function mockOptionsInvalidUniqueID()
    {
        return array('uniqueId' => ['/*/*76']);
    }

    /**
     *  SQL Injection Unique ID (Single)
     */

    public function testSQLInjectionUniqueID()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionUniqueID')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Unique ID Single Test
     */
    public function mockOptionsSQLInjectionUniqueID()
    {
        return array('uniqueId' => ['\';--']);
    }

    /********** Unique ID Multiple Tests *********/

    /**
     *  Test to get Employees by Unique ID (Multiple)
     */

    public function testEmployeeByUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Unique ID Multiple Employee Test
     */
    public function mockOptionsUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', 'test2', 'test3']);
    }

    /**
     * Mock Database Return for Multiple Employee Test
     */
    public function mockQueryMultiple()
    {
        return array(
            array(
                'pebempl_pidm' => 00000000,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2016-01-01',
                'szbuniq_unique_id' => 'TEST1',
                'szbuniq_banner_id' => '+00000000',
                'eclass_desc' => 'Faculty Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => 'AS',
                'faculty_rank_desc' => "Assistant Professor",
                'librarian_rank' => null,
                'librarian_rank_desc' => null,
                'tenure_code' => 'TT',
                'tenure_description' => 'Tenure Track',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => 'GSA',
                'grad_level_standing_desc' => "Grad School Level A",
                'grad_level_cert_date' => "2016-01-01",
                'grad_level_exp_date' => "2999-11-01",
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Microbiology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
            array(
                'pebempl_pidm' => 00000001,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2015-01-01',
                'szbuniq_unique_id' => 'TESTUSER2',
                'szbuniq_banner_id' => '+00000001',
                'eclass_desc' => 'Administrative Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'AT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => null,
                'faculty_rank_desc' => null,
                'librarian_rank' => 'ACL',
                'librarian_rank_desc' => 'Associate Librarian',
                'tenure_code' => 'T',
                'tenure_description' => 'Tenure',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => null,
                'grad_level_standing_desc' => null,
                'grad_level_cert_date' => null,
                'grad_level_exp_date' => null,
                'home_org_code' => 'ENG99',
                'home_org_description' => 'English',
                'standardized_division_code' => 'CAS',
                'standardized_division_name' => 'College of Arts and Science',
                'standardized_department_code' => 'EDL',
                'standardized_department_name' => 'Educational Leadership',
                'perfacc_facc_code' => 'DBIO',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
            array(
                'pebempl_pidm' => 00000002,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2013-01-20',
                'szbuniq_unique_id' => 'TESTUSER3',
                'szbuniq_banner_id' => '+00000002',
                'eclass_desc' => 'Administrative Full-time',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'ft_pt_group_code' => 'AF',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'faculty_rank' => 'PR',
                'faculty_rank_desc' => 'Professor',
                'librarian_rank' => null,
                'librarian_rank_desc' => null,
                'tenure_code' => 'NT',
                'tenure_description' => 'Not-tenurable',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => null,
                'grad_level_standing_desc' => null,
                'grad_level_cert_date' => null,
                'grad_level_exp_date' => null,
                'home_org_code' => 'ART99',
                'home_org_description' => 'Art',
                'standardized_division_code' => 'PHP',
                'standardized_division_name' => 'Physical Facilities',
                'standardized_department_code' => 'ENG',
                'standardized_department_name' => 'English',
                'perfacc_facc_code' => 'DBIO',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Mock Database Return for Total Multiple Employee Test
     */
    public function mockQueryTotalMultiple()
    {
        return array(
            array(
                'total' => 3,
            ),
        );
    }

    /**
     * Expected Return from Multiple Employee Test
     */
    public function returnEmployeeMultiple()
    {
        return array(
            array(
                "pidm" => "0",
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "groupCode" => null,
              "groupDescription" => null,
                "fullTimeCode" => "FT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "false",
                "rankCode" => "AS",
                "rankDescription" => "Assistant Professor",
                "tenureCode" => "TT",
                "tenureDescription" => "Tenure Track",
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => "GSA",
                "gradLevelDescription" => "Grad School Level A",
                "gradLevelCertDate" => "2016-01-01",
                "gradLevelExpDate" => "2999-11-01",
                'homeOrgCode' => 'BIO99',
                'homeOrgDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'dualAppointmentCode' => 'DENG',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null
            ),
            array(
                "pidm" => "1",
                "uniqueId" => "TESTUSER2",
                "plusNumber" => "+00000001",
                "startDate" => "2015-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Administrative Full-time",
                "groupCode" => null,
              "groupDescription" => null,
                "fullTimeCode" => "AT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "true",
                "rankCode" => "ACL",
                "rankDescription" => "Associate Librarian",
                "tenureCode" => 'T',
                "tenureDescription" => 'Tenure',
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => null,
                "gradLevelDescription" => null,
                "gradLevelCertDate" => null,
                "gradLevelExpDate" => null,
                'homeOrgCode' => 'ENG99',
                'homeOrgDescription' => 'English',
                'standardizedDivisionCode' => 'CAS',
                'standardizedDivisionName' => 'College of Arts and Science',
                'standardizedDepartmentCode' => 'EDL',
                'standardizedDepartmentName' => 'Educational Leadership',
                'dualAppointmentCode' => 'DBIO',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null

            ),
            array(
                "pidm" => "2",
                "uniqueId" => "TESTUSER3",
                "plusNumber" => "+00000002",
                "startDate" => "2013-01-20",
                "classificationCode" => "FF",
                "classificationDescription" => "Administrative Full-time",
                "groupCode" => null,
              "groupDescription" => null,
                "fullTimeCode" => "AF",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "false",
                "rankCode" => 'PR',
                "rankDescription" => 'Professor',
                "tenureCode" => 'NT',
                "tenureDescription" => 'Not-tenurable',
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => null,
                "gradLevelDescription" => null,
                "gradLevelCertDate" => null,
                "gradLevelExpDate" => null,
                'homeOrgCode' => 'ART99',
                'homeOrgDescription' => 'Art',
                'standardizedDivisionCode' => 'PHP',
                'standardizedDivisionName' => 'Physical Facilities',
                'standardizedDepartmentCode' => 'ENG',
                'standardizedDepartmentName' => 'English',
                'dualAppointmentCode' => 'DBIO',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null

            ),

        );
    }

    /**
     *  Invalid Unique ID (Multiple)
     */

    public function testInvalidUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Unique ID Single Test
     */
    public function mockOptionsInvalidUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', '/*/*76', 'test2']);
    }

    /**
     *  SQL Injection Unique ID (Multiple)
     */

    public function testSQLInjectionUniqueIDMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionUniqueIDMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Unique ID.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Unique ID Multiple Test
     */
    public function mockOptionsSQLInjectionUniqueIDMultiple()
    {
        return array('uniqueId' => ['test1', '\';--', 'test2']);
    }

    /**********************************/
    /**********PIDM Tests**************/
    /**********************************/

    /********** PIDM Single Tests *********/

    /**
     *  Test to get Employees by PIDM (Single)
     */

    public function testEmployeeByPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for PIDM Single Employee Test
     */
    public function mockOptionsPIDM()
    {
        return array('pidm' => ['00000000']);
    }

    /**
     *  Invalid PIDM (Single)
     */

    public function testInvalidPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid PIDM Single Test
     */
    public function mockOptionsInvalidPIDM()
    {
        return array('pidm' => ['test1']);
    }

    /**
     *  SQL Injection PIDM (Single)
     */

    public function testSQLInjectionPIDM()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionPIDM')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection PIDM Single Test
     */
    public function mockOptionsSQLInjectionPIDM()
    {
        return array('pidm' => ['\';--']);
    }

    /********** PIDM Multiple Tests *********/

    /**
     *  Test to get Employees by PIDM (Multiple)
     */

    public function testEmployeeByPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for PIDM Multiple Employee Test
     */
    public function mockOptionsPIDMMultiple()
    {
        return array('pidm' => ['00000000', '00000001', '00000002']);
    }

    /**
     *  Invalid PIDM (Multiple)
     */

    public function testInvalidPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid PIDM Single Test
     */
    public function mockOptionsInvalidPIDMMultiple()
    {
        return array('pidm' => ['00000000', 'test2', '00000002']);
    }

    /**
     *  SQL Injection PIDM (Multiple)
     */

    public function testSQLInjectionPIDMMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionPIDMMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid pidm.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection PIDM Multiple Test
     */
    public function mockOptionsSQLInjectionPIDMMultiple()
    {
        return array('pidm' => ['00000000', '\';--', '00000002']);
    }

    /********************************************/
    /**********Full Time Code Tests**************/
    /*******************************************/

    /********** Full Time Code Single Tests *********/

    /**
     *  Test to get Employees by Full Time Code (Single)
     */

    public function testEmployeeByFullTimeCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsFullTimeCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Full Time Code Single Employee Test
     */
    public function mockOptionsFullTimeCode()
    {
        return array('fullTimeCode' => ['FT']);
    }

    /**
     *  Invalid Full Time Code (Single)
     */

    public function testInvalidFullTimeCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidFullTimeCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Full-time Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Full Time Code Single Test
     */
    public function mockOptionsInvalidFullTimeCode()
    {
        return array('fullTimeCode' => ['12']);
    }

    /**
     *  SQL Injection Full Time Code (Single)
     */

    public function testSQLInjectionFullTimeCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionFullTimeCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Full-time Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Full Time Code Single Test
     */
    public function mockOptionsSQLInjectionFullTimeCode()
    {
        return array('fullTimeCode' => ['\';--']);
    }

    /********** Full Time Code Multiple Tests *********/

    /**
     *  Test to get Employees by Full Time Code (Multiple)
     */

    public function testEmployeeByFullTimeCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsFullTimeCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Full Time Code Multiple Employee Test
     */
    public function mockOptionsFullTimeCodeMultiple()
    {
        return array('fullTimeCode' => ['FT', 'AT', 'AF']);
    }


    /**
     *  Invalid Full Time Code (Multiple)
     */

    public function testInvalidFullTimeCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidFullTimeCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Full-time Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Full Time Code Single Test
     */
    public function mockOptionsInvalidFullTimeCodeMultiple()
    {
        return array('fullTimeCode' => ['FT', 'test2', 'PT']);
    }

    /**
     *  SQL Injection Full Time Code (Multiple)
     */

    public function testSQLInjectionFullTimeCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionFullTimeCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Full-time Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Full Time Code Multiple Test
     */
    public function mockOptionsSQLInjectionFullTimeCodeMultiple()
    {
        return array('fullTimeCode' => ['FT', '\';--', 'PT']);
    }

    /********************************************/
    /**********Grade Level Tests****************/
    /*******************************************/

    /********** Grade Level Single Tests *********/

    /**
     *  Test to get Employees by Grade Level (Single)
     */

    public function testEmployeeByGradeLevel()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsGradeLevel')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Grade Level Single Employee Test
     */
    public function mockOptionsGradeLevel()
    {
        return array('gradLevel' => ['GSA']);
    }

    /**
     *  Invalid Grade Level (Single)
     */

    public function testInvalidGradeLevel()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidGradeLevel')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Grad Level.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Grade Level Single Test
     */
    public function mockOptionsInvalidGradeLevel()
    {
        return array('gradLevel' => ['12']);
    }

    /**
     *  SQL Injection Grade Level (Single)
     */

    public function testSQLInjectionGradeLevel()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionGradeLevel')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Grad Level.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Grade Level Single Test
     */
    public function mockOptionsSQLInjectionGradeLevel()
    {
        return array('gradLevel' => ['\';--']);
    }

    /********** Grade Level Multiple Tests *********/

    /**
     *  Test to get Employees by Grade Level (Multiple)
     */

    public function testEmployeeByGradeLevelMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsGradeLevelMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Grade Level Multiple Employee Test
     */
    public function mockOptionsGradeLevelMultiple()
    {
        return array('gradLevel' => ['GSA', 'GPA', 'GSB']);
    }


    /**
     *  Invalid Grade Level (Multiple)
     */

    public function testInvalidGradeLevelMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidGradeLevelMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Grad Level.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Grade Level Single Test
     */
    public function mockOptionsInvalidGradeLevelMultiple()
    {
        return array('gradLevel' => ['GSA', '1235', 'none']);
    }

    /**
     *  SQL Injection Grade Level (Multiple)
     */

    public function testSQLInjectionGradeLevelMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionGradeLevelMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Grad Level.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Grade Level Multiple Test
     */
    public function mockOptionsSQLInjectionGradeLevelMultiple()
    {
        return array('gradLevel' => ['GSA', '\';--', 'GSB']);
    }

    /********************************************/
    /**********Tenure Tests****************/
    /*******************************************/

    /********** Tenure Single Tests *********/

    /**
     *  Test to get Employees by Tenure (Single)
     */

    public function testEmployeeByTenure()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsTenure')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Tenure Single Employee Test
     */
    public function mockOptionsTenure()
    {
        return array('tenureCode' => ['TT']);
    }

    /**
     *  Invalid Tenure (Single)
     */

    public function testInvalidTenure()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidTenure')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Tenure Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Tenure Single Test
     */
    public function mockOptionsInvalidTenure()
    {
        return array('tenureCode' => ['12']);
    }

    /**
     *  SQL Injection Tenure (Single)
     */

    public function testSQLInjectionTenure()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionTenure')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Tenure Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Tenure Single Test
     */
    public function mockOptionsSQLInjectionTenure()
    {
        return array('tenureCode' => ['\';--']);
    }

    /********** Tenure Multiple Tests *********/

    /**
     *  Test to get Employees by Tenure (Multiple)
     */

    public function testEmployeeByTenureMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsTenureMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Tenure Multiple Employee Test
     */
    public function mockOptionsTenureMultiple()
    {
        return array('tenureCode' => ['TT', 'T', 'NT']);
    }


    /**
     *  Invalid Tenure (Multiple)
     */

    public function testInvalidTenureMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidTenureMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Tenure Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Tenure Single Test
     */
    public function mockOptionsInvalidTenureMultiple()
    {
        return array('tenureCode' => ['NT', '1235', 'TT']);
    }

    /**
     *  SQL Injection Tenure (Multiple)
     */

    public function testSQLInjectionTenureMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionTenureMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Tenure Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Tenure Multiple Test
     */
    public function mockOptionsSQLInjectionTenureMultiple()
    {
        return array('tenureCode' => ['TT', '\';--', 'NT']);
    }

    /********************************************/
    /**********Rank Code Tests****************/
    /*******************************************/

    /********** Rank Code Single Tests *********/

    /**
     *  Test to get Employees by Rank Code (Single)
     */

    public function testEmployeeByRankCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsRankCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Rank Code Single Employee Test
     */
    public function mockOptionsRankCode()
    {
        return array('rankCode' => ['TT']);
    }

    /**
     *  Invalid Rank Code (Single)
     */

    public function testInvalidRankCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidRankCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Rank Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Rank Code Single Test
     */
    public function mockOptionsInvalidRankCode()
    {
        return array('rankCode' => ['12']);
    }

    /**
     *  SQL Injection Rank Code (Single)
     */

    public function testSQLInjectionRankCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionRankCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Rank Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Rank Code Single Test
     */
    public function mockOptionsSQLInjectionRankCode()
    {
        return array('rankCode' => ['\';--']);
    }

    /********** Rank Code Multiple Tests *********/

    /**
     *  Test to get Employees by Rank Code (Multiple)
     */

    public function testEmployeeByRankCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsRankCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Rank Code Multiple Employee Test
     */
    public function mockOptionsRankCodeMultiple()
    {
        return array('rankCode' => ['AS', 'ACL', 'PR']);
    }


    /**
     *  Invalid Rank Code (Multiple)
     */

    public function testInvalidRankCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidRankCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Rank Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Rank Code Single Test
     */
    public function mockOptionsInvalidRankCodeMultiple()
    {
        return array('rankCode' => ['NT', '1235', 'TT']);
    }

    /**
     *  SQL Injection Rank Code (Multiple)
     */

    public function testSQLInjectionRankCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionRankCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Rank Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Rank Code Multiple Test
     */
    public function mockOptionsSQLInjectionRankCodeMultiple()
    {
        return array('rankCode' => ['PR', '\';--', 'ACL']);
    }

    /********************************************/
    /*************Home Org Tests****************/
    /*******************************************/

    /********** Home Org Single Tests *********/

    /**
     *  Test to get Employees by Home Org (Single)
     */

    public function testEmployeeByHomeOrg()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsHomeOrg')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Home Org Single Employee Test
     */
    public function mockOptionsHomeOrg()
    {
        return array('homeOrgCode' => ['BIO99']);
    }

    /**
     *  Invalid Home Org (Single)
     */

    public function testInvalidHomeOrg()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidHomeOrg')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Home Org Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Home Org Single Test
     */
    public function mockOptionsInvalidHomeOrg()
    {
        return array('homeOrgCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Home Org (Single)
     */

    public function testSQLInjectionHomeOrg()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionHomeOrg')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Home Org Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Home Org Single Test
     */
    public function mockOptionsSQLInjectionHomeOrg()
    {
        return array('homeOrgCode' => ['\';--']);
    }

    /********** Home Org Multiple Tests *********/

    /**
     *  Test to get Employees by Home Org (Multiple)
     */

    public function testEmployeeByHomeOrgMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsHomeOrgMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Home Org Multiple Employee Test
     */
    public function mockOptionsHomeOrgMultiple()
    {
        return array('homeOrgCode' => ['BIO99', 'ENG99', 'ART99']);
    }


    /**
     *  Invalid Home Org (Multiple)
     */

    public function testInvalidHomeOrgMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidHomeOrgMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Home Org Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Home Org Single Test
     */
    public function mockOptionsInvalidHomeOrgMultiple()
    {
        return array('homeOrgCode' => ['BIO99', '.asd.a', 'ART99']);
    }

    /**
     *  SQL Injection Home Org (Multiple)
     */

    public function testSQLInjectionHomeOrgMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionHomeOrgMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Home Org Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Home Org Multiple Test
     */
    public function mockOptionsSQLInjectionHomeOrgMultiple()
    {
        return array('homeOrgCode' => ['BIO99', '\';--', 'ART99']);
    }

    /**************************************************************/
    /*************Standardized Division Code Tests****************/
    /************************************************************/

    /********** Standardized Division Code Single Tests *********/

    /**
     *  Test to get Employees by Standardized Division Code (Single)
     */

    public function testEmployeeByStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Standardized Division Code Single Employee Test
     */
    public function mockOptionsStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['FSB']);
    }

    /**
     *  Invalid Standardized Division Code (Single)
     */

    public function testInvalidStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standaridzed Division Code Single Test
     */
    public function mockOptionsInvalidStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Standardized Divsion (Single)
     */

    public function testSQLInjectionStandardizedDivisionCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDivisionCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Division Single Test
     */
    public function mockOptionsSQLInjectionStandardizedDivisionCode()
    {
        return array('standardizedDivisionCode' => ['\';--']);
    }

    /********** Standardized Division Code Multiple Tests *********/

    /**
     *  Test to get Employees by Standardized Division Code (Multiple)
     */

    public function testEmployeeByStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Standardized Division Code Multiple Employee Test
     */
    public function mockOptionsStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', 'CAS', 'PHP']);
    }


    /**
     *  Invalid Division (Multiple)
     */

    public function testInvalidStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Division Code Single Test
     */
    public function mockOptionsInvalidStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', '.asd.a', 'PHP']);
    }

    /**
     *  SQL Injection Standardized Division Code (Multiple)
     */

    public function testSQLInjectionStandardizedDivisionCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDivisionCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Division Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Division Code Multiple Test
     */
    public function mockOptionsSQLInjectionStandardizedDivisionCodeMultiple()
    {
        return array('standardizedDivisionCode' => ['FSB', '\';--', 'PHP']);
    }

    /**************************************************************/
    /*************Standardized Department Code Tests****************/
    /************************************************************/

    /********** Standardized Department Code Single Tests *********/

    /**
     *  Test to get Employees by Standardized Department Code (Single)
     */

    public function testEmployeeByStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Standardized Department Code Single Employee Test
     */
    public function mockOptionsStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['CSE']);
    }

    /**
     *  Invalid Standardized Department Code (Single)
     */

    public function testInvalidStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Department Code Single Test
     */
    public function mockOptionsInvalidStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Standardized Department (Single)
     */

    public function testSQLInjectionStandardizedDepartmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDepartmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Department Single Test
     */
    public function mockOptionsSQLInjectionStandardizedDepartmentCode()
    {
        return array('standardizedDepartmentCode' => ['\';--']);
    }

    /********** Standardized Department Code Multiple Tests *********/

    /**
     *  Test to get Employees by Standardized Department Code (Multiple)
     */

    public function testEmployeeByStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Standardized Department Code Multiple Employee Test
     */
    public function mockOptionsStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', 'EDL', 'ENG']);
    }


    /**
     *  Invalid Department (Multiple)
     */

    public function testInvalidStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Standardized Department Code Single Test
     */
    public function mockOptionsInvalidStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', '.asd.a', 'ENG']);
    }

    /**
     *  SQL Injection Standardized Department Code (Multiple)
     */

    public function testSQLInjectionStandardizedDepartmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionStandardizedDepartmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Standardized Department Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Standardized Department Code Multiple Test
     */
    public function mockOptionsSQLInjectionStandardizedDepartmentCodeMultiple()
    {
        return array('standardizedDepartmentCode' => ['CSE', '\';--', 'ENG']);
    }


    /**************************************************************/
    /*************Professional Librarian Code Tests****************/
    /************************************************************/

    /********** Professional Librarian Single Tests *********/

    /**
     * Mock Database Return for Professional Librarian Single Employee Test
     */
    public function mockQuerySingleTrueProfessonialLibrarian()
    {
        return array(
            array(
                'pebempl_pidm' => 0,
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2016-01-01',
                'szbuniq_unique_id' => 'TEST1',
                'szbuniq_banner_id' => '+00000000',
                'eclass_desc' => 'Faculty Full-time',
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'faculty_rank' => null,
                'faculty_rank_desc' => null,
                'librarian_rank' => 'ACL',
                'librarian_rank_desc' => 'Associate Librarian',
                'tenure_code' => 'TT',
                'tenure_description' => 'Tenure Track',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => 'GSA',
                'grad_level_standing_desc' => "Grad School Level A",
                'grad_level_cert_date' => "2016-01-01",
                'grad_level_exp_date' => "2999-11-01",
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Microbiology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Expected Return from  Professional Librarian Single Employee Test
     */
    public function returnEmployeeSingleTrueProfessonialLibrarian()
    {
        return array(
            array(
                "pidm" => 0,
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "groupCode" => null,
              "groupDescription" => null,
                "fullTimeCode" => "FT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "true",
                "rankCode" => "ACL",
                "rankDescription" => "Associate Librarian",
                "tenureCode" => "TT",
                "tenureDescription" => "Tenure Track",
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => "GSA",
                "gradLevelDescription" => "Grad School Level A",
                "gradLevelCertDate" => "2016-01-01",
                "gradLevelExpDate" => "2999-11-01",
                'homeOrgCode' => 'BIO99',
                'homeOrgDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'dualAppointmentCode' => 'DENG',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null
            )
        );
    }

    /**
     *  Test to get Employees by Professional Librarian Code True (Single)
     */

    public function testEmployeeByProfessionalLibrarianCodeTrue()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsProfessionalLibrarianCodeTrue')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingleTrueProfessonialLibrarian'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingleTrueProfessonialLibrarian());

    }

    /**
     * Mock Options for Professional Librarian Code Single Employee Test True
     */
    public function mockOptionsProfessionalLibrarianCodeTrue()
    {
        return array('professionalLibrarian' => 'true');
    }

    /**
     *  Test to get Employees by Professional Librarian Code False (Single)
     */

    public function testEmployeeByProfessionalLibrarianCodeFalse()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsProfessionalLibrarianCodeFalse')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Professional Librarian Code Single Employee Test False
     */
    public function mockOptionsProfessionalLibrarianCodeFalse()
    {
        return array('professionalLibrarian' => 'false');
    }

    /**
     *  Invalid Professional Librarian Code (Single)
     */

    public function testInvalidProfessionalLibrarianCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidProfessionalLibrarianCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid professionalLibrarian filter value.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Professional Librarian Code Single Test
     */
    public function mockOptionsInvalidProfessionalLibrarianCode()
    {
        return array('professionalLibrarian' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Professional Librarian Code (Single)
     */

    public function testSQLInjectionProfessionalLibrarianCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionProfessionalLibrarianCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid professionalLibrarian filter value.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Professional Librarian Code Single Test
     */
    public function mockOptionsSQLInjectionProfessionalLibrarianCode()
    {
        return array('professionalLibrarian' => ['\';--']);
    }

    /**************************************************************/
    /*************Dual Appointment Code Tests****************/
    /************************************************************/

    /********** Dual Appointment Code Single Tests *********/

    /**
     *  Test to get Employees by Dual Appointment Code (Single)
     */

    public function testEmployeeByDualAppointmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDualAppointmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options forDual Appointment Code Single Employee Test
     */
    public function mockOptionsDualAppointmentCode()
    {
        return array('dualAppointmentCode' => ['DENG']);
    }

    /**
     *  Invalid Dual Appointment Code (Single)
     */

    public function testInvalidDualAppointmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidDualAppointmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Dual Appointment Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Dual Appointment Code Single Test
     */
    public function mockOptionsInvalidDualAppointmentCode()
    {
        return array('dualAppointmentCode' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Dual Appointment (Single)
     */

    public function testSQLInjectionDualAppointmentCode()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionDualAppointmentCode')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Dual Appointment Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Dual Appointment Single Test
     */
    public function mockOptionsSQLInjectionDualAppointmentCode()
    {
        return array('dualAppointmentCode' => ['\';--']);
    }

    /********** Dual Appointment Code Multiple Tests *********/

    /**
     *  Test to get Employees by Dual Appointment Code (Multiple)
     */

    public function testEmployeeByDualAppointmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsDualAppointmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryMultiple'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalMultiple'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeMultiple());

    }

    /**
     * Mock Options for Dual Appointment Code Multiple Employee Test
     */
    public function mockOptionsDualAppointmentCodeMultiple()
    {
        return array('dualAppointmentCode' => ['DENG', 'DBIO']);
    }


    /**
     *  Invalid Dual Appointment Code (Multiple)
     */

    public function testInvalidDualAppointmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidDualAppointmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Dual Appointment Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Dual Appointment Code Single Test
     */
    public function mockOptionsInvalidDualAppointmentCodeMultiple()
    {
        return array('dualAppointmentCode' => ['DENG', '.asd.a', 'DBIO']);
    }

    /**
     *  SQL Injection Dual Appointment Code (Multiple)
     */

    public function testSQLInjectionDualAppointmentCodeMultiple()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionDualAppointmentCodeMultiple')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid Dual Appointment Code.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Dual Appointment Code Multiple Test
     */
    public function mockOptionsSQLInjectionDualAppointmentCodeMultiple()
    {
        return array('dualAppointmentCode' => ['DENG', '\';--', 'DBIO']);
    }

    /**************************************************************/
    /************* traditionalDepartment filter Tests****************/
    /************************************************************/

    /********** traditionalDepartmentSingle Tests *********/

    /**
     * Mock Database Return for traditionalDepartment Single Employee Test
     */
    public function mockQuerySingleTrueTraditionalDepartment()
    {
        return array(
            array(
                'pebempl_pidm' => '0',
                'pebempl_ecls_code' => 'FF',
                'start_date' => '2016-01-01',
                'szbuniq_unique_id' => 'TEST1',
                'szbuniq_banner_id' => '+00000000',
                'eclass_desc' => 'Faculty Full-time',
                'ft_pt_group_code' => 'FT',
                'ft_pt_group_desc' => 'Full-Time Employee',
                'pebempl_egrp_code' => null,
                'ptvegrp_desc' => null,
                'faculty_rank' => null,
                'faculty_rank_desc' => null,
                'librarian_rank' => 'ACL',
                'librarian_rank_desc' => 'Associate Librarian',
                'tenure_code' => 'TT',
                'tenure_description' => 'Tenure Track',
                'cip_code' => null,
                'cip_description' => null,
                'grad_level_standing_code' => 'GSA',
                'grad_level_standing_desc' => "Grad School Level A",
                'grad_level_cert_date' => "2016-01-01",
                'grad_level_exp_date' => "2999-11-01",
                'home_org_code' => 'BIO99',
                'home_org_description' => 'Microbiology',
                'standardized_division_code' => 'FBS',
                'standardized_division_name' => 'Farmer School of Business',
                'standardized_department_code' => 'CSE',
                'standardized_department_name' => 'Computer Science & Software Engineering',
                'perfacc_facc_code' => 'DENG',
                'first_hire_date' => null,
                'pebempl_coas_code_home' => null,
                'pebempl_coas_code_dist' => null,
                'pebempl_orgn_code_dist' => null,
                'pebempl_lcat_code' => null,
                'pebempl_bcat_code' => null,
                'service_date' => null,
                'seniority_date' => null,
                'pebempl_trea_code' => null,
                'activity_date' => null,
                'pebempl_wkpr_code' => null,
                'pebempl_flsa_ind' => null,
                'pebempl_internal_ft_pt_ind' => null,
                'pebempl_dicd_code' => null,
                'first_work_date' => null,
                'last_work_date' => null,
                'pebempl_jbln_code' => null,
                'pebempl_camp_code' => null,
                'pebempl_coll_code' => null,
                'rnum' => 1
            ),
        );
    }

    /**
     * Expected Return from  Traditional Department Single Employee Test
     */
    public function returnEmployeeSingleTrueTraditionalDepartment()
    {
        return array(
            array(
                "pidm" => "0",
                "uniqueId" => "TEST1",
                "plusNumber" => "+00000000",
                "startDate" => "2016-01-01",
                "classificationCode" => "FF",
                "classificationDescription" => "Faculty Full-time",
                "groupCode" => null,
                "groupDescription" => null,
                "fullTimeCode" => "FT",
                "fullTimeDescription" => "Full-Time Employee",
                "professionalLibrarian" => "true",
                "rankCode" => "ACL",
                "rankDescription" => "Associate Librarian",
                "tenureCode" => "TT",
                "tenureDescription" => "Tenure Track",
                "cipCode" => null,
                "cipDescription" => null,
                "gradLevelCode" => "GSA",
                "gradLevelDescription" => "Grad School Level A",
                "gradLevelCertDate" => "2016-01-01",
                "gradLevelExpDate" => "2999-11-01",
                'homeOrgCode' => 'BIO99',
                'homeOrgDescription' => 'Microbiology',
                'standardizedDivisionCode' => 'FBS',
                'standardizedDivisionName' => 'Farmer School of Business',
                'standardizedDepartmentCode' => 'CSE',
                'standardizedDepartmentName' => 'Computer Science & Software Engineering',
                'dualAppointmentCode' => 'DENG',
                'firstHireDate' => null,
                'chartOfAccountsCode' => null,
                'chartOfAccountsDistribution' => null,
                'orgCodeDistribution' => null,
                'leaveCategoryCode' => null,
                'benefitCategoryCode' => null,
                'serviceDate' => null,
                'seniorityDate' => null,
                'terminatedReasonCode' => null,
                'activityDate' => null,
                'workPeriodCode' => null,
                'flsaIndicator' => null,
                'internalFullTimePartTimeIndicator' => null,
                'districtCode' => null,
                'firstWorkDate' => null,
                'lastWorkDate' => null,
                'jobLocationCode' => null,
                'campusCode' => null,
                'collegeCode' => null
            )
        );
    }

    /**
     *  Test to get Employees by TraditionalDepartment True (Single)
     */

    public function testEmployeeByTraditionalDepartmentTrue()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsTraditionalDepartmentTrue')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingleTrueTraditionalDepartment'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingleTrueTraditionalDepartment());

    }

    /**
     * Mock Options for Professional Librarian Code Single Employee Test True
     */
    public function mockOptionsTraditionalDepartmentTrue()
    {
        return array('traditionalDepartment' => 'true');
    }

    /**
     *  Test to get Employees by Professional Librarian Code False (Single)
     */

    public function testEmployeeByTraditionalDepartmentFalse()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsTraditionalDepartmentFalse')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());

        //tell the dbh what to do when the queryall_array method is called.
        $this->dbh->expects($this->at(0))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQuerySingle'
            )));

        $this->dbh->expects($this->at(1))->method('queryall_array')
            ->will($this->returnCallback(array(
                $this,
                'mockQueryTotalSingle'
            )));

        $this->response = $this->employee->getEmployees();
        $payload = $this->response->getPayload();

        $this->assertEquals(App::API_OK, $this->response->getStatus());
        $this->assertEquals($payload, $this->returnEmployeeSingle());

    }

    /**
     * Mock Options for Professional Librarian Code Single Employee Test False
     */
    public function mockOptionsTraditionalDepartmentFalse()
    {
        return array('traditionalDepartment' => 'false');
    }

    /**
     *  Invalid Traditional Department(Single)
     */

    public function testInvalidTraditionalDepartment()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsInvalidTraditionalDepartment')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid traditionalDepartment filter value.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for Invalid Professional Librarian Code Single Test
     */
    public function mockOptionsInvalidTraditionalDepartment()
    {
        return array('traditionalDepartment' => ['.asdkna.']);
    }

    /**
     *  SQL Injection Traditional Department (Single)
     */

    public function testSQLInjectionTraditionalDepartment()
    {
        $this->request->method('getOptions')
            ->will($this->returnCallback(array($this, 'mockOptionsSQLInjectionTraditionalDepartment')));

        $this->request->method('getOffset')
            ->willReturn(1);

        $this->request->method('getLimit')
            ->willReturn(20);


        $this->request->method('getSubObjects')
            ->willReturn(array());


        try {
            $this->response = $this->employee->getEmployees();
            $this->assertTrue(false, "Error: should throw an exception");
        } catch (\Exception $e) {
            $this->assertStringStartsWith("Invalid traditionalDepartment filter value.",
                $e->getMessage());
        }

    }

    /**
     * Mock Options for SQL Injection Professional Librarian Code Single Test
     */
    public function mockOptionsSQLInjectionTraditionalDepartment()
    {
        return array('traditionalDepartment' => ['\';--']);
    }


}